var TeamworkGiftcardsStep = Class.create();
TeamworkGiftcardsStep.prototype = {
    initialize: function(form, saveUrl, skipUrl){
        this.form = form;
        if ($(this.form)) {
            $(this.form).observe('submit', function(event){this.save();Event.stop(event);}.bind(this));
        }
        this.saveUrl = saveUrl;
        this.skipUrl = skipUrl;
        this.validator = new Validation(this.form);
        this.onSave = this.nextStep.bindAsEventListener(this);
        this.onComplete = this.resetLoadWaiting.bindAsEventListener(this);
    },

    validate: function() {
        if(!this.validator.validate()) {
            return false;
        }
        return true;
    },

    save: function(){

        if (checkout.loadWaiting!=false) return;
        if (this.validate()) {
            checkout.setLoadWaiting('teamwork_giftcard_step');
            var request = new Ajax.Request(
                this.saveUrl,
                {
                    method:'post',
                    onComplete: this.onComplete,
                    onSuccess: this.onSave,
                    onFailure: checkout.ajaxFailure.bind(checkout),
                    parameters: Form.serialize(this.form)
                }
            );
        }
    },

    skip: function(){

        if (checkout.loadWaiting!=false) return;
        if (this.validate()) {
            checkout.setLoadWaiting('teamwork_giftcard_step');
            var request = new Ajax.Request(
                this.skipUrl,
                {
                    method:'post',
                    onComplete: this.onComplete,
                    onSuccess: this.onSave,
                    onFailure: checkout.ajaxFailure.bind(checkout),
                    parameters: Form.serialize(this.form)
                }
            );
        }
    },


    resetLoadWaiting: function(transport){
        checkout.setLoadWaiting(false);
    },

    nextStep: function(transport){
        if (transport && transport.responseText){
            try{
                response = eval('(' + transport.responseText + ')');
            }
            catch (e) {
                response = {};
            }
        }

        if (response.error) {
            alert(response.message);
            return false;
        }

        if (response.update_section) {
            $('checkout-'+response.update_section.name+'-load').update(response.update_section.html);
        }

        if (response.cardinfo_section) {
            $('card-info-other-content').update(response.cardinfo_section);
        }
        if (typeof response.cardno != "undefined") {
            $('teamwork_gifcards_giftcard_no').value = response.cardno;
        }

        if (response.goto_section) {
            checkout.gotoSection(response.goto_section);
            checkout.reloadProgressBlock();
            return;
        }

        checkout.setPayment();
    },

    getcardinfo: function(url) {
        var cardnumber = $('teamwork_gifcards_giftcard_no').value;
        $('teamwork_giftcard_step-getcardinfo-button').hide();
        $('teamwork_giftcard_step-getting-card-info-please-wait').show();
        $('teamwork_giftcard_step-skip-button').writeAttribute('disabled', 'disabled');
        $('teamwork_giftcard_step-skip-button').addClassName('disabled');
        $('teamwork_giftcard_step-continue-button').writeAttribute('disabled', 'disabled');
        $('teamwork_giftcard_step-continue-button').addClassName('disabled');
        $('teamwork_gifcards_giftcard_no').writeAttribute('disabled', 'disabled');

        var request = new Ajax.Request(
                url,
                {
                    method:'get',
                    onComplete: function(response) {
                                    /*show buttons*/
                                    $('teamwork_giftcard_step-getting-card-info-please-wait').hide();
                                    $('teamwork_giftcard_step-getcardinfo-button').show();
                                    $('teamwork_giftcard_step-continue-button').removeAttribute('disabled');
                                    $('teamwork_giftcard_step-skip-button').removeAttribute('disabled');
                                    $('teamwork_giftcard_step-skip-button').removeClassName('disabled');
                                    $('teamwork_giftcard_step-continue-button').removeClassName('disabled');
                                    $('teamwork_gifcards_giftcard_no').removeAttribute('disabled');
                                },
                    onSuccess:  function(transport) {
                                    //alert(response);

                                    if (transport && transport.responseText){
                                        try{
                                            response = eval('(' + transport.responseText + ')');
                                        }
                                        catch (e) {
                                            response = {};
                                        }
                                    }

                                    if (response.error){
                                           alert(response.message);
                                        return false;
                                    }
                                    /*update section*/
                                    if (response.cardinfo_section) {
                                        $('card-info-other-content').update(response.cardinfo_section);
                                    }

                                },
                    onFailure: function(){
                                    alert('Internal error occured');
                               },
                    parameters: 'giftcard_no=' + cardnumber
                }
            );
    }
}
