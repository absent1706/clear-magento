<?php
$installer = $this;
$installer->startSetup();
$tableName = $this->getTable('teamwork_giftcardssvsemu/giftcards_svsemu');

$installer->run("
DROP TABLE IF EXISTS `{$tableName}`;
CREATE TABLE `{$tableName}` (
  `entity_id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `card_no` VARCHAR(255) NOT NULL DEFAULT '',
  `fixed` BOOL DEFAULT FALSE,
  `amount` DECIMAL(12,4) NOT NULL DEFAULT 0,
  `comment` TEXT DEFAULT NULL,
  PRIMARY KEY (`entity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");

$installer->endSetup();
