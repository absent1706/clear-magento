<?php
class Teamwork_GiftcardsSVSEmu_Model_Card extends Mage_Core_Model_Abstract
{
    /**
     * Initialize resources
     */
    protected function _construct()
    {
        $this->_init('teamwork_giftcardssvsemu/card');
    }
}
