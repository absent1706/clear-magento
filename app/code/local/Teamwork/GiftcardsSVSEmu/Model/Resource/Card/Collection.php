<?php

class Teamwork_GiftcardsSVSEmu_Model_Resource_Card_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract//Mage_Core_Model_Resource_Db_Collection_Abstract
{
    protected function _construct()
    {
        $this->_init('teamwork_giftcardssvsemu/card');
    }
}
