<?php
class Teamwork_GiftcardsSVSEmu_Model_Rewrite_Svs extends Teamwork_Giftcards_Model_Svs
{

    public function _construct()
    {
        return;
    }

    protected function _getCard($giftcardNo)
    {
        $result = false;
        $cards = Mage::getModel('teamwork_giftcardssvsemu/card')->getCollection()->addFieldToFilter('card_no', $giftcardNo)->load();
        if (count($cards)) {
            $result = $cards->getFirstItem(); 
        }
        return $result;
    }
    
    public function isGiftcardExists($giftcardNo)
    {
        if ($this->_getCard($giftcardNo)) {
            return true;
        }
        return false;
    }

    public function giftcardBalance($giftcardNo)
    {
        if ($card = $this->_getCard($giftcardNo)) {
            return $card->getAmount(); 
        }
        return false;
    }

    public function refund($giftcardNo, $amount)
    {
        $res = $this->executeOperation($giftcardNo, $amount, false, 1);
        if ($res !== false && isset($res['transactionId'])) {
            return $res['transactionId'];
        }
        return 'Exception someException';
    }


    public function executeOperation($giftcardNo, $amount, $negative=true, $storeId)
    {
        if ($card = $this->_getCard($giftcardNo)) {
            if (!$card->getFixed()) {
                $amount = $this->negative($amount, $negative);
                $oldAmount = $card->getAmount();
                $card->setAmount($oldAmount + $amount);
                $card->save();
            }
            return array('transactionId'=>'sometransactionid', 'error'=>false);
        }
        return array('error'=>true);
    }

}
