<?php
class Teamwork_GiftcardsSVSEmu_Model_Rewrite_Payment extends Teamwork_Giftcards_Model_Payment
{
    public function refund(Varien_Object $payment, $amount, $comment = "")
    {
        $api = Mage::getSingleton('teamwork_giftcards/svs');
        $error = true;
        if($amount != $payment->getAmountOrdered())
        {
            $operation = $api->executeOperation($payment->getAdditionalInformation('GiftcardNo'), $amount, false, $payment->getOrder()->getStoreId());
            if(empty($operation['error']) && !empty($operation['transactionId']))
            {
                $error = false;
                $transactionId = $operation['transactionId'];
            }
        }
        else
        {
            //$response = $api->refund($payment->getRefundTransactionId(), $payment->getOrder()->getStoreId());
            $response = $api->refund($payment->getAdditionalInformation('GiftcardNo'), $amount);
            if(substr($response, 0, 9) != 'Exception')
            {
                $error = false;
                $transactionId = $response;
            }
        }

        if($error)
        {
            //Mage::getSingleton('teamwork_giftcards/payment_history')->addRecord($payment->getOrder()->getId(), Teamwork_Giftcards_Model_Payment_History_Record::RECORD_TYPE_ONLINE_REFUND, $amount, Teamwork_Giftcards_Model_Payment_History_Record::OPERATION_STATUS_FAIL, $this->getInfoInstance()->getAdditionalInformation('GiftcardNo'), "Please try later or contact us about the problem.");
            Mage::helper('teamwork_giftcards')->rememberOnlineOperation($payment->getOrder(), Teamwork_Giftcards_Model_Payment_History_Record::RECORD_TYPE_ONLINE_REFUND, $amount, Teamwork_Giftcards_Model_Payment_History_Record::OPERATION_STATUS_FAIL, $this->getInfoInstance()->getAdditionalInformation('GiftcardNo'), $comment . Mage::helper('teamwork_giftcards')->__(" Please try later or contact us about the problem."));
            Mage::throwException(Mage::helper('teamwork_giftcards')->__("Please try later or contact us about the problem. "));
        }
        else
        {
            $payment->setStatus(self::STATUS_APPROVED)
                ->setTransactionId((string)$transactionId)
            ->setIsTransactionClosed(0);
        }
        //Mage::getSingleton('teamwork_giftcards/payment_history')->addRecord($payment->getOrder()->getId(), Teamwork_Giftcards_Model_Payment_History_Record::RECORD_TYPE_ONLINE_REFUND, $amount, Teamwork_Giftcards_Model_Payment_History_Record::OPERATION_STATUS_SUCCESS, $this->getInfoInstance()->getAdditionalInformation('GiftcardNo'));
        Mage::helper('teamwork_giftcards')->rememberOnlineOperation($payment->getOrder(), Teamwork_Giftcards_Model_Payment_History_Record::RECORD_TYPE_ONLINE_REFUND, $amount, Teamwork_Giftcards_Model_Payment_History_Record::OPERATION_STATUS_SUCCESS, $this->getInfoInstance()->getAdditionalInformation('GiftcardNo'), $comment);
        return $this;
    }
}