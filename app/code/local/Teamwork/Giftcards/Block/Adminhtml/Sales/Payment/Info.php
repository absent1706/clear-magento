<?php

class Teamwork_Giftcards_Block_Adminhtml_Sales_Payment_Info extends Mage_Core_Block_Template
{
    protected $_childrenBlocks = array();

    public function _construct()
    {
        parent::_construct();
        $this->setTemplate('teamwork_giftcards/sales/payment/info.phtml');
    }

    public function init($collection)
    {
        $giftCardPaymentCode = Mage::helper('teamwork_giftcards')->getMethodCode();
        $lastBlock = false;
        foreach($collection as $payment)
        {
            $block = Mage::helper('payment')->getInfoBlock($payment);
            if ($payment->getMethod() == $giftCardPaymentCode)
            {
                $lastBlock = $block;
                $block->setData('__teamwork_giftcards_show_special_fields_', true);
                $infoInstance = $payment->getMethodInstance()->getInfoInstance();
                $unspentFunds = $infoInstance->getAdditionalInformation('UnspentFunds');
                $giftcardNo = $infoInstance->getAdditionalInformation('GiftcardNo');
                $unspentFundsHtml = Mage::helper('core')->currency($unspentFunds, true, false);
                $returnUrl = Mage::helper("adminhtml")->getUrl('teamwork_giftcards/adminhtml_sales_payment_info/returnbalance', array('order_id' => $payment->getOrder()->getId()));
                $button = $this->getLayout()->createBlock('adminhtml/widget_button')
                              ->setData(array(
                                'id'       => 'teamwork_giftcards_return_balance',
                                'label'    => Mage::helper('teamwork_giftcards')->__('Return the Balance'),
                                'class'    => 'save',
                                'on_click' => "deleteConfirm('Are you sure you want to return {$unspentFundsHtml} to Gift Card No {$giftcardNo} ?', '{$returnUrl}')",
                                'disabled' => (floatval($unspentFunds) <= 0),
                          ));

                $block->setChild('return_the_balance_button', $button);
                continue;
            }

            $this->_childrenBlocks[] = $block;
        }
        if ($lastBlock) $this->_childrenBlocks[] = $lastBlock;
    }

    public function getChildrenBlocks()
    {
        return $this->_childrenBlocks;
    }
}