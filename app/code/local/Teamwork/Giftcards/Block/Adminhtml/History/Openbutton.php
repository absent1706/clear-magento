<?php
class Teamwork_Giftcards_Block_Adminhtml_History_Openbutton extends Mage_Adminhtml_Block_System_Config_Form_Field
{

    protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element)
    {
        $this->setElement($element);

        $url = Mage::helper("adminhtml")->getUrl('teamwork_giftcards/adminhtml_history');

        $html = $this->getLayout()->createBlock('adminhtml/widget_button')
                    ->setType('button')
                    ->setClass('scalable')
                    ->setLabel($this->__('Open'))
                    ->setOnClick("setLocation('$url')")
                    ->toHtml();

        return $html;
    }
}