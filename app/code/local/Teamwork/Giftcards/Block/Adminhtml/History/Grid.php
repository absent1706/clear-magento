<?php

class Teamwork_Giftcards_Block_Adminhtml_History_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();

        // Set some defaults for our grid
        $this->setDefaultSort('id');
        $this->setId('teamwork_giftcards');
        $this->setDefaultDir('asc');
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection()
    {
        // Get and set our collection for the grid
        $collection = Mage::getResourceModel('teamwork_giftcards/payment_history_record_collection');
        $orderTableName = Mage::getSingleton('core/resource')->getTableName('sales/order');
        $collection->getSelect()->joinLeft(array('order' => $orderTableName), 'order.entity_id=order_id', array('order_increment_id' => 'order.increment_id'));
        $invoiceTableName = Mage::getSingleton('core/resource')->getTableName('sales/invoice');
        $collection->getSelect()->joinLeft(array('invoice' => $invoiceTableName), 'invoice.entity_id=invoice_id', array('invoice_increment_id' => 'invoice.increment_id'));

        $CreditmemoTableName = Mage::getSingleton('core/resource')->getTableName('sales/creditmemo');
        $collection->getSelect()->joinLeft(array('creditmemo' => $CreditmemoTableName), 'creditmemo.entity_id=creditmemo_id', array('creditmemo_increment_id' => 'creditmemo.increment_id'));

        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        // Add the columns that should appear in the grid
        $this->addColumn('entity_id',
            array(
                'header'=> $this->__('ID'),
                'align' =>'right',
                'width' => '50px',
                'index' => 'entity_id'
            )
        );

        $this->addColumn('gift_card_no',
            array(
                'header'=> $this->__('No'),
                'align' =>'right',
                'width' => '50px',
                'index' => 'gift_card_no'
            )
        );

        $this->addColumn('type',
            array(
                'header'=> Mage::helper('catalog')->__('Type'),
                'width' => '70px',
                'index' => 'type',
                'type'  => 'options',
                'align' =>'right',
                'options' => Mage::getModel('teamwork_giftcards/payment_history_record')->getTypeOptionArray(),
        ));

        $this->addColumn('order_increment_id',
            array(
                //'header'=> $this->__('Order Increment Id'),
                'header'=> $this->__('Order'),
                'align' =>'right',
                'width' => '50px',
                'index' => 'order_increment_id'
            )
        );

        $this->addColumn('invoice_increment_id',
            array(
                //'header'=> $this->__('Invoice Increment Id'),
                'header'=> $this->__('Invoice'),
                'align' =>'right',
                'width' => '50px',
                'index' => 'invoice_increment_id'
            )
        );

        $this->addColumn('creditmemo_increment_id',
            array(
                //'header'=> $this->__('Creditmemo Increment Id'),
                'header'=> $this->__('Creditmemo'),
                'align' =>'right',
                'width' => '50px',
                'index' => 'creditmemo_increment_id'
            )
        );

        $this->addColumn('status',
            array(
                'header'=> Mage::helper('catalog')->__('Status'),
                'width' => '70px',
                'index' => 'status',
                'type'  => 'options',
                'align' =>'right',
                'options' => Mage::getModel('teamwork_giftcards/payment_history_record')->getStatusOptionArray(),
        ));

        $store = $this->_getStore();
        $this->addColumn('amount',
            array(
                'header'=> $this->__('Amount'),
                'type'  => 'price',
                'currency_code' => $store->getBaseCurrency()->getCode(),
                'index' => 'amount',
        ));

        $this->addColumn('created_at', array(
            'header' => Mage::helper('sales')->__('Performed At'),
            'index' => 'created_at',
            'type' => 'datetime',
            'width' => '100px',
        ));

        $this->addColumn('comment',
            array(
                'header'=> $this->__('Comments'),
                //'align' =>'right',
                'width' => '200px',
                'index' => 'comment',
                'type' => 'text',
            )
        );

       /* $this->addColumn('name',
            array(
                'header'=> $this->__('Name'),
                'index' => 'name'
            )
        );*/

        return parent::_prepareColumns();
    }

    public function getRowUrl($row)
    {
        // This is where our row data will link to
       // return $this->getUrl('*/*/edit', array('id' => $row->getId()));
       return '#';
    }

    protected function _getStore()
    {
        $storeId = (int) $this->getRequest()->getParam('store', 0);
        return Mage::app()->getStore($storeId);
    }

}