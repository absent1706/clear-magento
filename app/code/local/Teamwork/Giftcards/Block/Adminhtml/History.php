<?php

class Teamwork_Giftcards_Block_Adminhtml_History extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    protected $_addButtonLabel = 'Add New Example';

    public function __construct()
    {
        parent::__construct();
        $this->_controller = 'adminhtml_history';
        $this->_blockGroup = 'teamwork_giftcards';
        $this->_headerText = Mage::helper('teamwork_giftcards')->__('Giftcard History');
        $this->_removeButton('add');

    }
}
