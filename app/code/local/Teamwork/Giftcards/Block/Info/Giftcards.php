<?php

class Teamwork_Giftcards_Block_Info_Giftcards extends Mage_Payment_Block_Info
{
    protected $_paymentSpecificInformation = null;

    protected function _prepareSpecificInformation($transport = null)
    {
        if (null === $this->_paymentSpecificInformation) {
            if (null === $transport) {
                $transport = new Varien_Object;
            } elseif (is_array($transport)) {
                $transport = new Varien_Object($transport);
            }

            Mage::dispatchEvent('payment_info_block_prepare_specific_information', array(
                'transport'     => $transport,
                'payment'       => $this->getInfo(),
                'block'            => $this,
            ));
            $transport->addData(array(
                Mage::helper('payment')->__('Giftcard No') => $this->getInfo()->getAdditionalInformation('GiftcardNo'),
                //Mage::helper('payment')->__('Amount') => $this->getInfo()->getAdditionalInformation('Amount')
            ));
            if ($this->getData('__teamwork_giftcards_show_special_fields_'))
            {
                $transport->addData(array(
                    Mage::helper('payment')->__('Withdrawn') => Mage::helper('core')->currency($this->getInfo()->getAdditionalInformation('Amount'),true,false),
                    Mage::helper('payment')->__('Not Applied') => Mage::helper('core')->currency($this->getInfo()->getAdditionalInformation('UnspentFunds'),true,false),
                    Mage::helper('payment')->__('Refunded') => Mage::helper('core')->currency($this->getInfo()->getAdditionalInformation('OnlineRefund'),true,false),
                ));
            }
            $this->_paymentSpecificInformation = $transport;
        }
        return $this->_paymentSpecificInformation;
    }

}
