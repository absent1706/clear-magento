<?php
class Teamwork_Giftcards_Block_Checkout_Onepage_Giftcardsstep_Formcontent extends Mage_Core_Block_Template
{

    public function getMethodCode()
    {
        return Mage::helper('teamwork_giftcards')->getMethodCode();
    }

    public function getCurentNo()
    {
        return $this->_getGiftCardInfo('GiftcardNo');
    }

    protected function _getGiftCardInfo($key)
    {

        $giftcardInfo = Mage::getSingleton('customer/session')->getData('teamwork_giftcard_info');
        if (isset($giftcardInfo[$key]))
        {
            $val = $giftcardInfo[$key];
        }
        else
        {
            //$val =  Mage::helper('teamwork_giftcards')->getGiftCardAdditionalInfo($key);
            $val =  Mage::getSingleton('checkout/session')->getQuote()->getGiftCardAdditionalInfo($key);
        }

        return $val;
    }

}
