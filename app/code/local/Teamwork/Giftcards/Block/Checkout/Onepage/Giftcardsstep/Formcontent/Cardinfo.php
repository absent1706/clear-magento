<?php
class Teamwork_Giftcards_Block_Checkout_Onepage_Giftcardsstep_Formcontent_Cardinfo extends Teamwork_Giftcards_Block_Checkout_Onepage_Giftcardsstep_Formcontent
{
    public function getCardBalance()
    {
        $cardBalance = $this->_getGiftCardInfo('card_balance');


        /*$session = Mage::getSingleton('customer/session');
        $giftCardInfo = array('card_balance'=>550, 'GiftcardNo'=>'some_test_1092737823647');

        $session->setData('teamwork_giftcard_info', $giftCardInfo);*/

        if (is_null($cardBalance) || $cardBalance === false)
        {
            $cardBalance = "<span class='red'>" . $this->__("Not available") . "</span>";
        }
        else
        {
            $cardBalance = Mage::helper('core')->currency($cardBalance, true, false);
        }
        return $cardBalance;
    }

    public function getCardBalanceAfter()
    {
        $cardBalance = $this->_getGiftCardInfo('card_balance');
        if (!is_null($cardBalance) &&  $cardBalance !== false)
        {
            $newCardBalance = $cardBalance - $this->_getCartGrandTotal();
            if ($newCardBalance < 0) $newCardBalance = 0;
            $newCardBalance = Mage::helper('core')->currency($newCardBalance, true, false);
        }
        else
        {
            $newCardBalance = "<span class='red'>" . $this->__("Not available") . "</span>";
        }
        return $newCardBalance;
    }


    public function getCartGrandtotal()
    {
        return Mage::helper('core')->currency($this->_getCartGrandTotal(), true, false);
    }


    public function _getCartGrandTotal()
    {
        return Mage::getSingleton('checkout/cart')->getQuote()->getGrandTotal();
    }


}
