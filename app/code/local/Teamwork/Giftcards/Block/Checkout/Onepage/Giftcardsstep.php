<?php
class Teamwork_Giftcards_Block_Checkout_Onepage_Giftcardsstep extends Mage_Checkout_Block_Onepage_Abstract
{
    protected function _construct()
    {
        $this->getCheckout()->setStepData('teamwork_giftcard_step', array(
            'label'     => Mage::helper('checkout')->__('Gift Card'),
            'is_show'   => $this->isShow()
        ));
        //if ($this->isCustomerLoggedIn()) {
            $this->getCheckout()->setStepData('teamwork_giftcard_step', 'allow', false);
            //$this->getCheckout()->setStepData('billing', 'allow', false);
        //}

        parent::_construct();
    }
}
