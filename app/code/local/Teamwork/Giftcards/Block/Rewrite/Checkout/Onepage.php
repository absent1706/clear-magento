<?php

class Teamwork_Giftcards_Block_Rewrite_Checkout_Onepage extends Mage_Checkout_Block_Onepage
{
    protected function _getStepCodes()
    {
        return array('login', 'billing', 'shipping', 'shipping_method', 'teamwork_giftcard_step', 'payment', 'review');
    }
    /*fix for magento 1.5*/
    public function getSteps()
    {
        $steps = array();
        $stepCodes = $this->_getStepCodes();

        if ($this->isCustomerLoggedIn()) {
            $stepCodes = array_diff($stepCodes, array('login'));
        }

        foreach ($stepCodes as $step) {
            $steps[$step] = $this->getCheckout()->getStepData($step);
        }

        return $steps;
    }
    
}
