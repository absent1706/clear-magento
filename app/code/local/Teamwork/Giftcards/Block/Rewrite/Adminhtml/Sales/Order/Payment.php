<?php

class Teamwork_Giftcards_Block_Rewrite_Adminhtml_Sales_Order_Payment extends Mage_Adminhtml_Block_Sales_Order_Payment
{

    public function setPayment($payment)
    {
        $paymentsCollection = $payment->getOrder()->getPaymentsCollection();
        if (count($paymentsCollection) > 1)
        {
            $paymentInfoBlock = $this->getLayout()->createBlock('teamwork_giftcards/adminhtml_sales_payment_info');
            $paymentInfoBlock->init($paymentsCollection);
        }
        else
        {
            $paymentInfoBlock = Mage::helper('payment')->getInfoBlock($payment);
        }
        $this->setChild('info', $paymentInfoBlock);
        $this->setData('payment', $payment);

        return $this;
    }

}
