<?php

class Teamwork_Giftcards_Block_List extends Mage_Core_Block_Text
{
    protected function _toHtml()
    {
        $this->setText('');
        foreach ($this->_children as $block) {
            $this->addText($block->toHtml());
        }
        return parent::_toHtml();
    }
}