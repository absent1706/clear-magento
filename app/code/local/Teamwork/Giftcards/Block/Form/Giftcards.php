<?php
class Teamwork_Giftcards_Block_Form_Giftcards extends Mage_Payment_Block_Form
{
    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('teamwork_giftcards/form/giftcards.phtml');
    }
}
