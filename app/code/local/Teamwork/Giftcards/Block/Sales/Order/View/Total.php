<?php

class Teamwork_Giftcards_Block_Sales_Order_View_Total extends Mage_Core_Block_Template
{

    public function initTotals()
    {

        if ($giftCardPaymentObj = $this->getParentBlock()->getOrder()->getGiftCardPaymenMethod(true))
        {
            $amount = $giftCardPaymentObj->getMethodInstance()->getInfoInstance()->getAdditionalInformation('Amount');

            $giftcardAmountTotal = new Varien_Object(array(
                'code'  => 'teamwork_giftcards_amount',
                'label' => $this->__('Gift Card Amount'),
                'value' => $amount,
            ));
            $this->getParentBlock()->addTotal($giftcardAmountTotal);
        }

        return $this;

    }

}
