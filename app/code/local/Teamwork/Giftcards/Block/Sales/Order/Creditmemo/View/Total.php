<?php

class Teamwork_Giftcards_Block_Sales_Order_Creditmemo_View_Total extends Mage_Core_Block_Template
{

    protected $_giftRefundedToGiftCard = array();

    public function initTotals()
    {
        $amount = $this->_getGiftCardAmount();
        if ($amount !== false)
        {
            $giftcardPaidTotal = new Varien_Object(array(
                'code'      => 'teamwork_giftcards_amount',
                'label' => $this->__('Returned to Gift Card'),
                'value' => $amount,
            ));
            $this->getParentBlock()->addTotal($giftcardPaidTotal);
        }
        return $this;
    }

    protected function _getGiftCardAmount()
    {

        $creditmemo = $this->getParentBlock()->getCreditmemo();
        $creditmemoId = $creditmemo->getId();
        if (!isset($this->_giftRefundedToGiftCard[$creditmemoId]))
        {
            $historyCollection = Mage::getResourceModel('teamwork_giftcards/payment_history_record_collection');
            $historyCollection->addFieldToFilter('creditmemo_id', $creditmemoId)->load();
            if (count($historyCollection))
            {
                $this->_giftRefundedToGiftCard[$creditmemoId] = $historyCollection->getFirstItem()->getAmount();
            }
            else
            {
                $order = $creditmemo->getOrder();
                if ($order->getGiftCardPaymenMethod())
                {
                    $this->_giftRefundedToGiftCard[$creditmemoId] = 0;
                }
                else
                {
                    $this->_giftRefundedToGiftCard[$creditmemoId] = false;
                }
            }
        }
        return $this->_giftRefundedToGiftCard[$creditmemoId];

    }

}
