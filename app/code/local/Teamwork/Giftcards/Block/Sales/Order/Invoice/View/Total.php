<?php

class Teamwork_Giftcards_Block_Sales_Order_Invoice_View_Total extends Mage_Core_Block_Template
{

    protected $_giftCardPaidAmount = array();

    public function initTotals()
    {
        $amount = $this->_getGiftCardAmount();
        if ($amount !== false)
        {
            $giftcardPaidTotal = new Varien_Object(array(
                'code'      => 'teamwork_giftcards_amount',
                'label' => $this->__('Paid by Gift Card'),
                'value' => $amount,
            ));
            $this->getParentBlock()->addTotal($giftcardPaidTotal);
        }
        return $this;
    }

    protected function _getGiftCardAmount()
    {
        $invoice = $this->getParentBlock()->getInvoice();
        $invoiceId = $invoice->getId();
        if (!isset($this->_giftCardPaidAmount[$invoiceId]))
        {
            $historyCollection = Mage::getResourceModel('teamwork_giftcards/payment_history_record_collection');
            $historyCollection->addFieldToFilter('invoice_id', $invoiceId)->load();
            if (count($historyCollection))
            {
                $this->_giftCardPaidAmount[$invoiceId] = $historyCollection->getFirstItem()->getAmount();
            }
            else
            {
                $order = $invoice->getOrder();
                if ($order->getGiftCardPaymenMethod())
                {
                    $this->_giftCardPaidAmount[$invoiceId] = 0;
                }
                else
                {
                    $this->_giftCardPaidAmount[$invoiceId] = false;
                }
            }
        }
        return $this->_giftCardPaidAmount[$invoiceId];
    }

}
