<?php

require_once Mage::getModuleDir('controllers', 'Mage_Checkout') . DS . 'OnepageController.php';

class Teamwork_Giftcards_Rewrite_Checkout_OnepageController extends Mage_Checkout_OnepageController
{

    /**
     * save checkout billing address
     */
    public function saveBillingAction()
    {
        if ($this->_expireAjax()) {
            return;
        }
        if ($this->getRequest()->isPost()) {
//            $postData = $this->getRequest()->getPost('billing', array());
//            $data = $this->_filterPostData($postData);
            $data = $this->getRequest()->getPost('billing', array());
            $customerAddressId = $this->getRequest()->getPost('billing_address_id', false);

            if (isset($data['email'])) {
                $data['email'] = trim($data['email']);
            }
            $result = $this->getOnepage()->saveBilling($data, $customerAddressId);

            if (!isset($result['error'])) {
                /* check quote for virtual */
                if ($this->getOnepage()->getQuote()->isVirtual()) {
                    $result['goto_section'] = 'teamwork_giftcard_step';
                    $result['update_section'] = array(
                        'name' => 'teamwork_giftcard_step',
                        'html' => $this->_getTeamworkGiftcardsHtml()
                    );
                } elseif (isset($data['use_for_shipping']) && $data['use_for_shipping'] == 1) {
                    $result['goto_section'] = 'shipping_method';
                    $result['update_section'] = array(
                        'name' => 'shipping-method',
                        'html' => $this->_getShippingMethodsHtml()
                    );

                    $result['allow_sections'] = array('shipping');
                    $result['duplicateBillingInfo'] = 'true';
                } else {
                    $result['goto_section'] = 'shipping';
                }
            }

            $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
        }
    }


    /**
     * Shipping method save action
     */
    public function saveShippingMethodAction()
    {
        if ($this->_expireAjax()) {
            return;
        }
        if ($this->getRequest()->isPost()) {
            $data = $this->getRequest()->getPost('shipping_method', '');
            $result = $this->getOnepage()->saveShippingMethod($data);
            /*
            $result will have erro data if shipping method is empty
            */
            if(!$result) {
                Mage::dispatchEvent('checkout_controller_onepage_save_shipping_method',
                        array('request'=>$this->getRequest(),
                            'quote'=>$this->getOnepage()->getQuote()));
                $this->getOnepage()->getQuote()->collectTotals();
                $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));

                $result['goto_section'] = 'teamwork_giftcard_step';
                $result['update_section'] = array(
                    'name' => 'teamwork_giftcard_step',
                    'html' => $this->_getTeamworkGiftcardsHtml()
                );
            }
            $this->getOnepage()->getQuote()->collectTotals()->save();
            $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
        }
    }


    /**
     * Create order action
     */
    public function saveOrderAction()
    {
        if ($this->_expireAjax()) {
            return;
        }

        $result = array();
        try {
            if ($requiredAgreements = Mage::helper('checkout')->getRequiredAgreementIds()) {
                $postedAgreements = array_keys($this->getRequest()->getPost('agreement', array()));
                if ($diff = array_diff($requiredAgreements, $postedAgreements)) {
                    $result['success'] = false;
                    $result['error'] = true;
                    $result['error_messages'] = $this->__('Please agree to all the terms and conditions before placing the order.');
                    $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
                    return;
                }
            }

            /*fix: save payment data if payment is not GC (to use, for example, creditcard no etc.)*/
            if ( ($data = $this->getRequest()->getPost('payment', false))
                 && $this->getOnepage()->getQuote()->getPayment()->getMethod() !== Mage::helper('teamwork_giftcards')->getMethodCode()) {
                    $this->getOnepage()->getQuote()->getPayment()->importData($data);
            }

            //fix to prevent payment info deleting from quote
            $this->getOnepage()->getQuote()->collectTotals();//->save();
            $this->getOnepage()->saveOrder();

            $redirectUrl = $this->getOnepage()->getCheckout()->getRedirectUrl();
            $result['success'] = true;
            $result['error']   = false;

            /*remove giftcard info from session*/
            Mage::getSingleton('customer/session')->setData('teamwork_giftcard_info', array());
            
        } catch (Mage_Payment_Model_Info_Exception $e) {
            $message = $e->getMessage();
            if( !empty($message) ) {
                $result['error_messages'] = $message;
            }
            $result['goto_section'] = 'teamwork_giftcard_step';
            $result['update_section'] = array(
                'name' => 'teamwork_giftcard_step',
                'html' => $this->_getTeamworkGiftcardsHtml()
            );
        } catch (Mage_Core_Exception $e) {
            Mage::logException($e);
            Mage::helper('checkout')->sendPaymentFailedEmail($this->getOnepage()->getQuote(), $e->getMessage());
            $result['success'] = false;
            $result['error'] = true;
            $result['error_messages'] = $e->getMessage();

            if ($gotoSection = $this->getOnepage()->getCheckout()->getGotoSection()) {
                $result['goto_section'] = $gotoSection;
                $this->getOnepage()->getCheckout()->setGotoSection(null);
            }

            if ($updateSection = $this->getOnepage()->getCheckout()->getUpdateSection()) {
                if (isset($this->_sectionUpdateFunctions[$updateSection])) {
                    $updateSectionFunction = $this->_sectionUpdateFunctions[$updateSection];
                    $result['update_section'] = array(
                        'name' => $updateSection,
                        'html' => $this->$updateSectionFunction()
                    );
                }
                $this->getOnepage()->getCheckout()->setUpdateSection(null);
            }
        } catch (Exception $e) {
            Mage::logException($e);
            Mage::helper('checkout')->sendPaymentFailedEmail($this->getOnepage()->getQuote(), $e->getMessage());
            $result['success']  = false;
            $result['error']    = true;
            $result['error_messages'] = $this->__('There was an error processing your order. Please contact us or try again later.');
        }
        $this->getOnepage()->getQuote()->save();
        /**
         * when there is redirect to third party, we don't want to save order yet.
         * we will save the order in return action.
         */
        if (isset($redirectUrl)) {
            $result['redirect'] = $redirectUrl;
        }

        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
    }

    /**
     * Get teamwork giftcard step html
     *
     * @return string
     */
    protected function _getTeamworkGiftcardsHtml()
    {
        $layout = $this->getLayout();
        $update = $layout->getUpdate();
        $update->load('checkout_onepage_teamwork_giftcards_step_content');
        $layout->generateXml();
        $layout->generateBlocks();
        $output = $layout->getOutput();
        return $output;
    }

   public function savePaymentAction()
    {
        if ($this->_expireAjax()) {
            return;
        }
        $giftCardPaymentObj = false;
        $quote = $this->getOnepage()->getQuote();
        try {
            if (false && !$this->getRequest()->isPost()) {
            //    $this->_ajaxRedirectResponse();
                return;
            }


            $errorMessage = "";
            $errorFields = false;
            $redirectUrl = false;

            // set payment to quote
           // $result = array();
            $data = $this->getRequest()->getPost('payment', array());
         //   $data = array('method' => 'checkmo');
            //$result = $this->getOnepage()->savePayment($data);

            /*remove all payment methods but giftcard from quote*/
            $giftCardPaymentObj = $quote->removePayments(false);


            $payment = Mage::getModel('sales/quote_payment');

            $quote->addPayment($payment);
            $payment->importData($data);

            $quote->collectTotals()->save();

            // get section and redirect data
            $redirectUrl = $payment->getCheckoutRedirectUrl();


            // get section and redirect data
            /*$redirectUrl = $this->getOnepage()->getQuote()->getPayment()->getCheckoutRedirectUrl();
            if (empty($result['error']) && !$redirectUrl) {
                $this->loadLayout('checkout_onepage_review');
                $result['goto_section'] = 'review';
                $result['update_section'] = array(
                    'name' => 'review',
                    'html' => $this->_getReviewHtml()
                );
            }
            if ($redirectUrl) {
                $result['redirect'] = $redirectUrl;
            }*/
        } catch (Mage_Payment_Exception $e) {
            if ($e->getFields()) {
                //$result['fields'] = $e->getFields();
                $errorFields = $e->getFields();
            }
            //$result['error'] = $e->getMessage();
            $errorMessage = $e->getMessage();
        } catch (Mage_Core_Exception $e) {
            //$result['error'] = $e->getMessage();
            $errorMessage = $e->getMessage();
        } catch (Exception $e) {
            Mage::logException($e);
//            $result['error'] = $this->__('Unable to set Payment Method.');
            $errorMessage = $this->__('Unable to set Payment Method.');
        }


        if (!$redirectUrl)
        {
            if (!$errorMessage)
            {
                $this->getOnepage()->getCheckout()
                    ->setStepData('payment', 'allow', true)
                    ->setStepData('payment', 'complete', true);


                $this->loadLayout('checkout_onepage_review');
                $result['goto_section'] = 'review';
                $result['update_section'] = array(
                    'name' => 'review',
                    'html' => $this->_getReviewHtml()
                );

            }
            else
            {
                $result['success']  = false;
                $result['error']    = true;
                $result['message'] = $errorMessage;
                if ($errorFields) $result['fields'] = $errorFields;
            }
        }
        else
        {
            $result['redirect'] = $redirectUrl;
        }

        if ($giftCardPaymentObj && !$errorMessage)
        {
            /**
             * Save grand total in giftcard payment additional info.
             * The all grandtotal changing from this point are unacceptable if 2 payment methods set (or will be set)
             * */
            $quote->setGiftCardAdditionalInfo('GrandTotal', $quote->getGrandTotal());
        }

        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
    }

}
