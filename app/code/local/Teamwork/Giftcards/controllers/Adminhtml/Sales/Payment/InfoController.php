<?php
class Teamwork_Giftcards_Adminhtml_Sales_Payment_InfoController extends Mage_Adminhtml_Controller_Action
{
    public function returnbalanceAction()
    {
        $route = 'adminhtml/sales_order/index';
        $routeParam = array();
        try
        {
            $orderId = $this->getRequest()->getParam('order_id', false);

            if (!$orderId)
            {
                throw new Exception($this->__('Wrong request.'));
            }

            $order = Mage::getModel('sales/order')->load($orderId);
            if ($order && $order->getId())
            {
                $route = 'adminhtml/sales_order/view';
                $routeParam['order_id'] = $orderId;

                $giftCardPaymentCode = Mage::helper('teamwork_giftcards')->getMethodCode();
                $paymentCollection = $order->getPaymentsCollection();
                $giftcardPayment = false;
                foreach($paymentCollection as $payment)
                {
                    if ($payment->getMethod() == $giftCardPaymentCode)
                    {
                        $giftcardPayment = $payment;
                        break;
                    }
                }
                if (!$giftcardPayment)
                {
                    throw new Exception($this->__('No gift card used'));
                }

                $infoInstance = $giftcardPayment->getMethodInstance()->getInfoInstance();
                $unspentFunds = floatval($infoInstance->getAdditionalInformation('UnspentFunds'));
                if ($unspentFunds <= 0)
                {
                    throw new Exception($this->__('Nothing to return'));
                }
                $giftcardPayment->getMethodInstance()->refund($giftcardPayment, $unspentFunds);
                $infoInstance->setAdditionalInformation('UnspentFunds', 0);

                $infoInstance->setAdditionalInformation('UnspentFunds', 0);

                $infoInstance->setAdditionalInformation('OnlineRefund', floatval($infoInstance->getAdditionalInformation('OnlineRefund')) + floatval($unspentFunds));



                $infoInstance->save();
                $this->_getSession()->addSuccess($this->__('Balance returned to gift card'));
            }
            else
            {
                throw new Exception($this->__('Wrong request.'));
            }
        }
        catch (Exception $e)
        {
            $this->_getSession()->addError($e->getMessage());
        }
        $this->_redirect($route, $routeParam);
    }
}