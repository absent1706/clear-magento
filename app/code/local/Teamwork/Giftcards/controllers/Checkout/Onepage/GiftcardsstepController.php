<?php

require_once Mage::getModuleDir('controllers', 'Teamwork_Giftcards') . DS . 'Rewrite' . DS . 'Checkout' . DS . 'OnepageController.php';

class Teamwork_Giftcards_Checkout_Onepage_GiftcardsstepController extends Teamwork_Giftcards_Rewrite_Checkout_OnepageController
{
    public function saveAction()
    {

        if ($this->_expireAjax())
        {
            return;
        }
        if (false && !$this->getRequest()->isPost())
        {
            $this->_ajaxRedirectResponse();
            return;
        }
        $data = $this->getRequest()->getPost('payment', array());

       //   $data = array('method' => 'teamwork_giftcards', 'giftcard_no' => 'sasasasa'/*, 'amount' => '300000'*/);

        $errorMessage = "";
        $errorFields = false;
        $nextStep = 'payment';

        $quote = $this->getOnepage()->getQuote();

        if (!empty($data['method']) && !empty($data['giftcard_no'])/* && !empty($data['amount'])*/)
        {

            /*remove all payment methods from quote*/
            $giftCardPaymentObj = $quote->removePayments(false);

            /*update session data*/
            $api = Mage::getSingleton('teamwork_giftcards/svs');
            $exists = $api->isGiftcardExists($data['giftcard_no']);
            $balance = false;
            if ($exists)
            {
                $balance = $api->giftcardBalance($data['giftcard_no']);
                $giftCardInfo = array('card_balance' => $balance, 'GiftcardNo' => $data['giftcard_no']);
                Mage::getSingleton('customer/session')->setData('teamwork_giftcard_info', $giftCardInfo);
            }
            else
            {
                $errorMessage = $this->__("Gift Card Does Not Exist");
            }

            if (!$errorMessage)
            {

                $data['amount'] = $balance;

                $grandTotal = $quote->getGrandTotal();
                if ($data['amount'] > $grandTotal)
                {
                    $data['amount'] = $grandTotal;
                }

                if ($data['amount'] >= $grandTotal)
                {
                    $nextStep = 'review';
                }

                try {

                    if ($giftCardPaymentObj === false) $payment = $quote->getPayment();
                    else $payment = $giftCardPaymentObj;
                    $payment->importData($data);
                    $giftCardPaymentCode = Mage::helper('teamwork_giftcards')->getMethodCode();

                    if ($quote->isVirtual())
                    {
                        $quote->getBillingAddress()->setPaymentMethod($giftCardPaymentCode);
                    }
                    else
                    {
                        $quote->getShippingAddress()->setPaymentMethod($giftCardPaymentCode);
                        $quote->getBillingAddress()->setPaymentMethod(null);
                    }
                    // shipping totals may be affected by payment method
                    if (!$quote->isVirtual() && $quote->getShippingAddress()) {
                        $quote->getShippingAddress()->setCollectShippingRates(true);
                    }
                    $quote->setTotalsCollectedFlag(false);
                    $quote->collectTotals()->save();
                    $payment->save();


                    $this->getOnepage()->getCheckout()
                                        ->setStepData('teamwork_giftcard_step', 'allow', true)
                                        ->setStepData('teamwork_giftcard_step', 'complete', true);

                    /**
                     * Save grand total in giftcard payment additional info.
                     * The all grandtotal changing from this point are unacceptable if 2 payment methods set (or will be set)
                     * */
                    $quote->setGiftCardAdditionalInfo('GrandTotal', $grandTotal);



                } catch (Mage_Payment_Exception $e) {
                    if ($e->getFields()) {
                        $errorFields = $e->getFields();
                    }
                    $errorMessage = $e->getMessage();
                } catch (Mage_Core_Exception $e) {
                    $errorMessage = $e->getMessage();
                } catch (Exception $e) {
                    Mage::logException($e);
                    $errorMessage = $this->__('Unable to set Payment Method.');
                }
            }
        }
        else
        {
            /*update session's card data */
            $giftCardInfo = array('GiftcardNo' => '');
            Mage::getSingleton('customer/session')->setData('teamwork_giftcard_info', $giftCardInfo);
            $quote->removePayments();
            $this->getOnepage()->getCheckout()
                               ->setStepData('teamwork_giftcard_step', 'complete', false);
        }

        if (!$errorMessage)
        {

            $result['cardinfo_section'] = $this->_getCardInfoHtml();

            if ($nextStep == 'payment')
            {

                $result['goto_section'] = 'payment';
                $result['update_section'] = array(
                        'name' => 'payment-method',
                        'html' => $this->_getPaymentMethodsHtml()
                );

                $this->getOnepage()->getCheckout()
                //                 ->setStepData('payment', 'allow', false)
                                   ->setStepData('payment', 'complete', false);

            }
            else if ($nextStep == 'review') /*skip payment method selection*/
            {
                $this->loadLayout('checkout_onepage_review');
                $result['goto_section'] = 'review';
                $result['update_section'] = array(
                        'name' => 'review',
                        'html' => $this->_getReviewHtml()
                );

            }
        }
        else
        {
            $result['success']  = false;
            $result['error']    = true;
            $result['message'] = $errorMessage;
            if ($errorFields) $result['fields'] = $errorFields;
        }


        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));

    }



    public function getcardinfoAction()
    {

        $giftCardNo = $this->getRequest()->getParam('giftcard_no', false);
        $errorMessage = "";
        if ($giftCardNo === false)
        {
            $errorMessage = $this->__("Wrong request");
        }
        $result = array();
        $api = Mage::getSingleton('teamwork_giftcards/svs');
        $exists = $api->isGiftcardExists($giftCardNo);
        $balance = false;
        if ($exists)
        {
            $balance = $api->giftcardBalance($giftCardNo);
        }
        else
        {
            $errorMessage = $this->__("Gift Card Does Not Exist");
        }

        if ($errorMessage)
        {
            $result['error'] = true;
            $result['message'] = $errorMessage;
        }
        else
        {
            /*save data in session*/
            $giftCardInfo = array('card_balance' => $balance, 'GiftcardNo' => $giftCardNo);
            Mage::getSingleton('customer/session')->setData('teamwork_giftcard_info', $giftCardInfo);
            /*and render info block*/
            $result['cardinfo_section'] = $this->_getCardInfoHtml();
        }

        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
    }

    protected function _getCardInfoHtml()
    {
        $layout = $this->getLayout();
        $update = $layout->getUpdate();

        /*fix: drop update cache to render fresh giftcard html*/
        Mage::app()->saveCache(null, $update->getCacheId());

        $update->load('teamwork_giftcards_step_cardinfo');
        $layout->generateXml();
        $layout->generateBlocks();
        $output = $layout->getOutput();

        /*reset layout to get clean payment and review step contents (see _getPaymentMethodsHtml and _getReviewHtml methods)*/
        $usedBlocks = array_keys($layout->getAllBlocks());
        foreach($usedBlocks as $blockName)
        {
            $layout->removeOutputBlock($blockName);
            $layout->unsetBlock($blockName);
        }
        $update->resetHandles();
        $update->resetUpdates();

        /*fix: drop update cache to render fresh payment html*/
        Mage::app()->saveCache(null, $update->getCacheId());

        return $output;
    }

    public function skipAction()
    {
        $this->getOnepage()->getQuote()->removePayments(true);

        $result['goto_section'] = 'payment';

        /*remove giftcard info from session*/
        Mage::getSingleton('customer/session')->setData('teamwork_giftcard_info', array());
        /*rerender payment step*/
        $result['cardinfo_section'] = $this->_getCardInfoHtml();
        /*remove data from giftcard no field*/
        $result['cardno'] = "";


        $result['update_section'] = array(
            'name' => 'payment-method',
            'html' => $this->_getPaymentMethodsHtml()
        );

        $this->getOnepage()->getCheckout()
        //                  ->setStepData('payment', 'allow', false)
                            ->setStepData('payment', 'complete', false);

        $this->getOnepage()->getCheckout()
        //                  ->setStepData('teamwork_giftcard_step', 'allow', false)
                            ->setStepData('teamwork_giftcard_step', 'complete', false);



        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));

    }

}
