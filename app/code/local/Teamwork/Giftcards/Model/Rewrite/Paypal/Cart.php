<?php

class Teamwork_Giftcards_Model_Rewrite_Paypal_Cart extends Mage_Paypal_Model_Cart
{

    /**
     * Check the line items and totals according to PayPal business logic limitations
     */
    protected function _validate()
    {
        $this->_areItemsValid = false;
        $this->_areTotalsValid = false;


        $referenceAmount = $this->_salesEntity->getBaseGrandTotal();

        /*remove gc amount*/
        $gcAmount = false;
        if ($this->_salesEntity instanceof Mage_Sales_Model_Quote)
        {
            $gcAmount = $this->_salesEntity->getGiftCardAdditionalInfo("Amount");
        }
        else if ($this->_salesEntity instanceof Mage_Sales_Model_Order)
        {
            if ($giftCardPayment = $this->_salesEntity->getGiftCardPaymenMethod())
            {
                $infoinstance = $giftCardPayment->getMethodInstance()->getInfoInstance();
                $gcAmount = $infoinstance->getAdditionalInformation('Amount');
            }
        }
        if ($gcAmount) $referenceAmount -= $gcAmount;

        $itemsSubtotal = 0;
        foreach ($this->_items as $i) {
            $itemsSubtotal = $itemsSubtotal + $i['qty'] * $i['amount'];
        }
        $sum = $itemsSubtotal + $this->_totals[self::TOTAL_TAX];
        if (!$this->_isShippingAsItem) {
            $sum += $this->_totals[self::TOTAL_SHIPPING];
        }
        if (!$this->_isDiscountAsItem) {
            $sum -= $this->_totals[self::TOTAL_DISCOUNT];
        }
        /**
         * numbers are intentionally converted to strings because of possible comparison error
         * see http://php.net/float
         */
        // match sum of all the items and totals to the reference amount
        if (sprintf('%.4F', $sum) == sprintf('%.4F', $referenceAmount)) {
            $this->_areItemsValid = true;
        }

        // PayPal requires to have discount less than items subtotal
        if (!$this->_isDiscountAsItem) {
            $this->_areTotalsValid = round($this->_totals[self::TOTAL_DISCOUNT], 4) < round($itemsSubtotal, 4);
        } else {
            $this->_areTotalsValid = $itemsSubtotal > 0.00001;
        }

        $this->_areItemsValid = $this->_areItemsValid && $this->_areTotalsValid;
    }

}
