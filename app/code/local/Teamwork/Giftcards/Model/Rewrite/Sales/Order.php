<?php

class Teamwork_Giftcards_Model_Rewrite_Sales_Order extends Mage_Sales_Model_Order
{

    /**
     * Retrieve order payment model object
     *
     * @return Mage_Sales_Model_Order_Payment
     */
    public function getPayment()
    {
        $paymentCollection = $this->getPaymentsCollection();
        if (count($this->getPaymentsCollection()) > 1)
        {
            $giftCardPaymentCode = Mage::helper('teamwork_giftcards')->getMethodCode();
            foreach ($this->getPaymentsCollection() as $payment)
            {
                if (!$payment->isDeleted() && $payment->getMethod() !== $giftCardPaymentCode)
                {
                    return $payment;
                }
            }
        }
        else
        {
            return parent::getPayment();
        }
        return false;
    }

    public function getGiftCardPaymenMethod($maybeSingle = false)
    {
        $paymentCollection = $this->getPaymentsCollection();
        $giftCardPayment = false;
        $giftCardPaymentCode = Mage::helper('teamwork_giftcards')->getMethodCode();
        if (count($paymentCollection) > 1 || $maybeSingle)
        {
            foreach($paymentCollection as $payment)
            {
                if ($payment->getMethod() == $giftCardPaymentCode)
                {
                    $giftCardPayment = $payment;
                    break;
                }
            }
        }
        return $giftCardPayment;
    }

    public function theOnlyGiftCardPaymentInUse()
    {
        $result = false;
        $paymentCollection = $this->getPaymentsCollection();
        if (count($paymentCollection) == 1
            && $paymentCollection->getFirstItem()->getMethod() == Mage::helper('teamwork_giftcards')->getMethodCode())
        {
            $result = true;
        }
        return $result;
    }


}
