<?php

class Teamwork_Giftcards_Model_Rewrite_Sales_Order_Payment extends Mage_Sales_Model_Order_Payment
{
    /**
     * Authorize payment either online or offline (process auth notification)
     * Updates transactions hierarchy, if required
     * Prevents transaction double processing
     * Updates payment totals, updates order status and adds proper comments
     *
     * @param bool $isOnline
     * @param float $amount
     * @return Mage_Sales_Model_Order_Payment
     */
    protected function _authorize($isOnline, $amount)
    {
        /*if this is not a gift card check if we have gift card and capture them if needed*/
        $paidByGiftCard = $this->payByGiftCard($isOnline);

        // update totals
        $amount = $this->_formatAmount($amount, true);
        $this->setBaseAmountAuthorized($amount);
        $amount = $this->_formatAmount($amount - $paidByGiftCard, true);


        // do authorization
        $order  = $this->getOrder();
        $state  = Mage_Sales_Model_Order::STATE_PROCESSING;
        $status = true;
        if ($isOnline) {
            // invoke authorization on gateway
            $this->getMethodInstance()->setStore($order->getStoreId())->authorize($this, $amount);
        }

        // similar logic of "payment review" order as in capturing
        if ($this->getIsTransactionPending()) {
            $message = Mage::helper('sales')->__('Authorizing amount of %s is pending approval on gateway.', $this->_formatPrice($amount));
            $state = Mage_Sales_Model_Order::STATE_PAYMENT_REVIEW;
            if ($this->getIsFraudDetected()) {
                $status = Mage_Sales_Model_Order::STATUS_FRAUD;
            }
        } else {
            $message = Mage::helper('sales')->__('Authorized amount of %s.', $this->_formatPrice($amount));
        }

        // update transactions, order state and add comments
        $transaction = $this->_addTransaction(Mage_Sales_Model_Order_Payment_Transaction::TYPE_AUTH);
        if ($order->isNominal()) {
            $message = $this->_prependMessage(Mage::helper('sales')->__('Nominal order registered.'));
        } else {
            $message = $this->_prependMessage($message);
            $message = $this->_appendTransactionToMessage($transaction, $message);
        }
        $order->setState($state, $status, $message);

        return $this;
    }

    /**
     * Capture the payment online
     * Requires an invoice. If there is no invoice specified, will automatically prepare an invoice for order
     * Updates transactions hierarchy, if required
     * Updates payment totals, updates order status and adds proper comments
     *
     * TODO: eliminate logic duplication with registerCaptureNotification()
     *
     * @return Mage_Sales_Model_Order_Payment
     * @throws Mage_Core_Exception
     */
    public function capture($invoice)
    {
        if (is_null($invoice)) {
            $invoice = $this->_invoice();
            $this->setCreatedInvoice($invoice);
            return $this; // @see Mage_Sales_Model_Order_Invoice::capture()
        }
        $amountToCapture = $this->_formatAmount($invoice->getBaseGrandTotal());
        $order = $this->getOrder();

        // prepare parent transaction and its amount
        $paidWorkaround = 0;
        if (!$invoice->wasPayCalled()) {
            $paidWorkaround = (float)$amountToCapture;
        }
        $this->_isCaptureFinal($paidWorkaround);

        $this->_generateTransactionId(
            Mage_Sales_Model_Order_Payment_Transaction::TYPE_CAPTURE,
            $this->getAuthorizationTransaction()
        );

        Mage::dispatchEvent('sales_order_payment_capture', array('payment' => $this, 'invoice' => $invoice));

        /**
         * Fetch an update about existing transaction. It can determine whether the transaction can be paid
         * Capture attempt will happen only when invoice is not yet paid and the transaction can be paid
         */
        if ($invoice->getTransactionId()) {
            $this->getMethodInstance()
                ->setStore($order->getStoreId())
                ->fetchTransactionInfo($this, $invoice->getTransactionId());
        }
        $status = true;
        if (!$invoice->getIsPaid() && !$this->getIsTransactionPending()) {
            // attempt to capture: this can trigger "is_transaction_pending"

           // $this->getMethodInstance()->setStore($order->getStoreId())->capture($this, $amountToCapture);
           // $paidByGiftCard = $this->payByGiftCard(true);
            //$amountToCapture_ = $this->_formatAmount($amountToCapture - $paidByGiftCard);
            $amountToCapture_ = $this->payByGiftCard(true, $amountToCapture);
            if ($amountToCapture_)
            {
                $invoice->setData('___tw_giftcards_used_by_capture_', true);
                $this->getMethodInstance()->setStore($order->getStoreId())->capture($this, $amountToCapture_);
                //$this->paidByOtherMethod($amountToCapture_);
            }

            $transaction = $this->_addTransaction(
                Mage_Sales_Model_Order_Payment_Transaction::TYPE_CAPTURE,
                $invoice,
                true
            );

            if ($this->getIsTransactionPending()) {
                $message = Mage::helper('sales')->__('Capturing amount of %s is pending approval on gateway.', $this->_formatPrice($amountToCapture));
                $state = Mage_Sales_Model_Order::STATE_PAYMENT_REVIEW;
                if ($this->getIsFraudDetected()) {
                    $status = Mage_Sales_Model_Order::STATUS_FRAUD;
                }
                $invoice->setIsPaid(false);
            } else { // normal online capture: invoice is marked as "paid"
                $message = Mage::helper('sales')->__('Captured amount of %s online.', $this->_formatPrice($amountToCapture));
                $state = Mage_Sales_Model_Order::STATE_PROCESSING;
                $invoice->setIsPaid(true);
                $this->_updateTotals(array('base_amount_paid_online' => $amountToCapture));
            }
            if ($order->isNominal()) {
                $message = $this->_prependMessage(Mage::helper('sales')->__('Nominal order registered.'));
            } else {
                $message = $this->_prependMessage($message);
                $message = $this->_appendTransactionToMessage($transaction, $message);
            }
            $order->setState($state, $status, $message);
            $this->getMethodInstance()->processInvoice($invoice, $this); // should be deprecated
            return $this;
        }
        Mage::throwException(
            Mage::helper('sales')->__('The transaction "%s" cannot be captured yet.', $invoice->getTransactionId())
        );
    }

    public function payByGiftCard($isOnline, $cribe = false)
    {
        if (!$isOnline) return 0;
        $giftCardPaymentCode = Mage::helper('teamwork_giftcards')->getMethodCode();
        $paidByGiftCard = 0;
        if ($this->getMethod() !== $giftCardPaymentCode)
        {
            $instance = $this->_getGiftcardsPaymentInstance();
            if ($instance)
            {
                $infoInstance = $instance->getInfoInstance();
                $giftCardAmount = $infoInstance->getAdditionalInformation('Amount');
                $giftCardPaid = $infoInstance->getAdditionalInformation('Paid');
                if (!$giftCardPaid && $giftCardAmount)
                {
                    $instance->capture($this->getOrder()->getGiftCardPaymenMethod(), $this->_formatAmount($giftCardAmount, true));
                    $infoInstance->setAdditionalInformation('Paid', true);
                    $infoInstance->setAdditionalInformation('UnspentFunds', $giftCardAmount);
                    $infoInstance->setAdditionalInformation('OfflineRefund', 0);
                    $infoInstance->save();
                }

                if ($cribe > 0)
                {
                    $unspentFunds = $infoInstance->getAdditionalInformation('UnspentFunds');
                    $nUnspentFunds = $unspentFunds - $cribe;
                    if ($nUnspentFunds < 0) $nUnspentFunds = 0;
                    $infoInstance->setAdditionalInformation('UnspentFunds', $nUnspentFunds);

                    //$infoInstance->setAdditionalInformation('LastOfflinePaymentAmount', floatval($unspentFunds) - floatval($nUnspentFunds));

                    $remains = $cribe - $unspentFunds;
                    if ($remains < 0) $remains = 0;
                    if ($nUnspentFunds < $unspentFunds) Mage::helper('teamwork_giftcards')->rememberOfflineOperation($this->getOrder()->getId(), Teamwork_Giftcards_Model_Payment_History_Record::RECORD_TYPE_OFFLINE_PAY, $unspentFunds - $nUnspentFunds, Teamwork_Giftcards_Model_Payment_History_Record::OPERATION_STATUS_SUCCESS, $infoInstance->getAdditionalInformation('GiftcardNo'));
                    if ($remains > 0)
                    {
                        $paidByOtherMethod = floatval($infoInstance->getAdditionalInformation('PaidByOtherMethod'));
                        $paidByOtherMethod += $remains;
                        $infoInstance->setAdditionalInformation('PaidByOtherMethod', $paidByOtherMethod);
                    }
                    return $remains;
                }

                //$infoInstance->setAdditionalInformation('LastOfflinePaymentAmount', 0);

                $paidByGiftCard = $giftCardAmount;
            }

        }
        else
        {
            return $cribe;
        }
        return $paidByGiftCard;
    }


    public function refund($creditmemo)
    {
        $baseAmountToRefund = $creditmemo->getBaseGrandTotal();

        $gatewayRefundAmount = $this->_formatAmount($this->refundToGiftCard($baseAmountToRefund));

        $baseAmountToRefund = $this->_formatAmount($baseAmountToRefund);
        $order = $this->getOrder();

        $this->_generateTransactionId(Mage_Sales_Model_Order_Payment_Transaction::TYPE_REFUND);

        // call refund from gateway if required
        $isOnline = false;
        $gateway = $this->getMethodInstance();
        $invoice = null;
        if ($gateway->canRefund() && $creditmemo->getDoTransaction()) {
            $this->setCreditmemo($creditmemo);
            $invoice = $creditmemo->getInvoice();
            if ($invoice) {
                $isOnline = true;
                $captureTxn = $this->_lookupTransaction($invoice->getTransactionId());
                if ($captureTxn) {
                    $this->setParentTransactionId($captureTxn->getTxnId());
                }
                $this->setShouldCloseParentTransaction(true); // TODO: implement multiple refunds per capture
                try {
                    $gateway->setStore($this->getOrder()->getStoreId())
                        ->processBeforeRefund($invoice, $this);
                    if ($gatewayRefundAmount > 0)
                    {
                        $gateway->refund($this, $gatewayRefundAmount);
                    }
                    $gateway->processCreditmemo($creditmemo, $this);
                } catch (Mage_Core_Exception $e) {
                    if (!$captureTxn) {
                        $e->setMessage(' ' . Mage::helper('sales')->__('If the invoice was created offline, try creating an offline creditmemo.'), true);
                    }
                    throw $e;
                }
            }
        }

        // update self totals from creditmemo
        $this->_updateTotals(array(
            'amount_refunded' => $creditmemo->getGrandTotal(),
            'base_amount_refunded' => $baseAmountToRefund,
            'base_amount_refunded_online' => $isOnline ? $baseAmountToRefund : null,
            'shipping_refunded' => $creditmemo->getShippingAmount(),
            'base_shipping_refunded' => $creditmemo->getBaseShippingAmount(),
        ));

        // update transactions and order state
        $transaction = $this->_addTransaction(
            Mage_Sales_Model_Order_Payment_Transaction::TYPE_REFUND,
            $creditmemo,
            $isOnline
        );
        if ($invoice) {
            $message = Mage::helper('sales')->__('Refunded amount of %s online.', $this->_formatPrice($baseAmountToRefund));
        } else {
            $message = $this->hasMessage() ? $this->getMessage()
                : Mage::helper('sales')->__('Refunded amount of %s offline.', $this->_formatPrice($baseAmountToRefund));
        }
        $message = $message = $this->_prependMessage($message);
        $message = $this->_appendTransactionToMessage($transaction, $message);
        $order->setState(Mage_Sales_Model_Order::STATE_PROCESSING, true, $message);

        Mage::dispatchEvent('sales_order_payment_refund', array('payment' => $this, 'creditmemo' => $creditmemo));
        return $this;
    }


    public function refundToGiftCard($refundRequestAmount)
    {
        $giftCardPaymentCode = Mage::helper('teamwork_giftcards')->getMethodCode();
        $needRefundToGC = 0;
        if ($this->getMethod() !== $giftCardPaymentCode)
        {
            $instance = $this->_getGiftcardsPaymentInstance();
            if ($instance)
            {
                $infoInstance = $instance->getInfoInstance();
                $giftCardAmount = $infoInstance->getAdditionalInformation('Amount');
                $giftCardPaid = $infoInstance->getAdditionalInformation('Paid');
                if ($giftCardPaid && $giftCardAmount)
                {
                    $paidByOtherMethod = floatval($infoInstance->getAdditionalInformation('PaidByOtherMethod'));
                    /*the first we need to return money to "other method" (!)*/
                    $needRefundToGC = $refundRequestAmount - $paidByOtherMethod;
                    if ($needRefundToGC < 0) $needRefundToGC = 0;

                    if ($needRefundToGC > 0)
                    {
                        $unspentFunds = $infoInstance->getAdditionalInformation('UnspentFunds');
                        $maxAllowedRefund = $giftCardAmount - $unspentFunds;
                        if ($needRefundToGC > $maxAllowedRefund)
                        {
                            $needRefundToGC = $maxAllowedRefund;
                        }
                        $infoInstance->setAdditionalInformation('UnspentFunds', $unspentFunds + $needRefundToGC);

                        $alreadyRefunded = $infoInstance->getAdditionalInformation('OfflineRefund');
                        $infoInstance->setAdditionalInformation('OfflineRefund', $alreadyRefunded + $needRefundToGC);
                        Mage::helper('teamwork_giftcards')->rememberOfflineOperation($this->getOrder()->getId(), Teamwork_Giftcards_Model_Payment_History_Record::RECORD_TYPE_OFFLINE_REFUND, $needRefundToGC, Teamwork_Giftcards_Model_Payment_History_Record::OPERATION_STATUS_SUCCESS, $infoInstance->getAdditionalInformation('GiftcardNo'));
                    }
                    if ($paidByOtherMethod)
                    {
                        $newPaidByOtherMethod = $paidByOtherMethod - ($refundRequestAmount - $needRefundToGC);
                        if ($newPaidByOtherMethod < 0) $newPaidByOtherMethod = 0;
                        $infoInstance->setAdditionalInformation('PaidByOtherMethod', $newPaidByOtherMethod);
                    }
                }
            }
        }
        return $refundRequestAmount - $needRefundToGC;
    }

    protected function _getGiftcardsPaymentInstance()
    {
        $instance = false;

        /*$order  = $this->getOrder();
        $paymentCollection = $order->getPaymentsCollection();
        $giftCardPayment = false;
        $giftCardPaymentCode = Mage::helper('teamwork_giftcards')->getMethodCode();
        if (count($paymentCollection) > 1)
        {
            foreach($paymentCollection as $payment)
            {
                if ($payment->getMethod() == $giftCardPaymentCode)
                {
                    $giftCardPayment = $payment;
                    break;
                }
            }
        }*/


        if ($giftCardPayment = $this->getOrder()->getGiftCardPaymenMethod())
        {
            $instance = $giftCardPayment->getMethodInstance();
        }
        return $instance;
    }


    /**
     * Order payment either online
     * Updates transactions hierarchy, if required
     * Prevents transaction double processing
     * Updates payment totals, updates order status and adds proper comments
     *
     * @param float $amount
     * @return Mage_Sales_Model_Order_Payment
     */
    protected function _order($amount)
    {
        /*if this is not a gift card check if we have gift card and capture them if needed*/
        $paidByGiftCard = $this->payByGiftCard(true);

        // update totals
        $amount = $this->_formatAmount($amount, true);
        $amount = $this->_formatAmount($amount - $paidByGiftCard, true);


        // do ordering
        $order  = $this->getOrder();
        $state  = Mage_Sales_Model_Order::STATE_PROCESSING;
        $status = true;
        $this->getMethodInstance()->setStore($order->getStoreId())->order($this, $amount);

        if ($this->getSkipOrderProcessing()) {
            return $this;
        }

        // similar logic of "payment review" order as in capturing
        if ($this->getIsTransactionPending()) {
            $message = Mage::helper('sales')->__('Ordering amount of %s is pending approval on gateway.', $this->_formatPrice($amount));
            $state = Mage_Sales_Model_Order::STATE_PAYMENT_REVIEW;
            if ($this->getIsFraudDetected()) {
                $status = Mage_Sales_Model_Order::STATUS_FRAUD;
            }
        } else {
            $message = Mage::helper('sales')->__('Ordered amount of %s.', $this->_formatPrice($amount));
        }

        // update transactions, order state and add comments
        $transaction = $this->_addTransaction(Mage_Sales_Model_Order_Payment_Transaction::TYPE_ORDER);
        $message = $this->_prependMessage($message);
        $message = $this->_appendTransactionToMessage($transaction, $message);
        $order->setState($state, $status, $message);
        return $this;
    }

}
