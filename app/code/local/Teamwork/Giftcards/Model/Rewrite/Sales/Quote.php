<?php

class Teamwork_Giftcards_Model_Rewrite_Sales_Quote extends Mage_Sales_Model_Quote
{

    /**
     * @return Mage_Sales_Model_Quote_Payment
     */
    public function getPayment()
    {
        if (!$result = $this->_getPayment(false))
        {
            $result = parent::getPayment();
        }
        return $result;
/*        $paymentCollection = $this->getPaymentsCollection();
        if (count($paymentCollection) > 1)
        {
            $giftCardPaymentCode = Mage::helper('teamwork_giftcards')->getMethodCode();
            foreach ($this->getPaymentsCollection() as $payment) {
                if (!$payment->isDeleted() && $payment->getMethod() !== $giftCardPaymentCode) {
                    return $payment;
                }
            }
        }
        return parent::getPayment();*/
    }



    public function getGiftCardAdditionalInfo($key)
    {
        $result = false;
        if ($payment = $this->getGiftCardPayment())
        {
            $instance = $payment->getMethodInstance();
            $infoInstance = $instance->getInfoInstance();
            $result = $infoInstance->getAdditionalInformation($key);
        }
        return $result;
    }

    public function setGiftCardAdditionalInfo($key, $val, $doSave = true)
    {
        $result = false;
        if ($payment = $this->getGiftCardPayment())
        {
            $instance = $payment->getMethodInstance();
            $infoInstance = $instance->getInfoInstance();
            $infoInstance->setAdditionalInformation($key, $val);
            if ($doSave) $infoInstance->save();
            $result = true;
        }
        return $result;
    }

    public function getGiftCardPayment()
    {
        return $this->_getPayment(true);
       /* $result = false;
        $paymentCollection = $this->getPaymentsCollection();
        if (count($paymentCollection))
        {
            $giftCardPaymentCode = Mage::helper('teamwork_giftcards')->getMethodCode();
            foreach($paymentCollection as $payment)
            {
                if (!$payment->isDeleted() && $payment->getMethod() == $giftCardPaymentCode)
                {
                    return $payment;
                }
            }
        }
        return false;*/
    }

    protected function _getPayment($giftCard = false)
    {
        $paymentCollection = $this->getPaymentsCollection();
        if (count($paymentCollection))
        {
            $giftCardPaymentCode = Mage::helper('teamwork_giftcards')->getMethodCode();
            foreach($paymentCollection as $payment)
            {
                if (!$payment->isDeleted())
                {
                    if (!($giftCard xor ($payment->getMethod() == $giftCardPaymentCode)))
                    {
                        return $payment;
                    }
                }
            }
        }
        return false;
    }



   public function removePayments($theAll = true)
    {
        /*remove all payment methods from quote*/
        $paymentCollection = $this->getPaymentsCollection();
        $giftCardPaymentObj = false;

        if (count($paymentCollection))
        {
            $giftCardPaymentCode = Mage::helper('teamwork_giftcards')->getMethodCode();
            $doDelete = false;
            foreach($paymentCollection as $payment)
            {
                if (!$payment->isDeleted())
                {
                    if ($theAll || $payment->getMethod() != $giftCardPaymentCode || $giftCardPaymentObj !== false)
                    {
                        $payment->isDeleted(true);
                        $doDelete = true;
                    }
                    else
                    {
                        $giftCardPaymentObj = $payment;
                    }
                }
            }
            if ($doDelete) $paymentCollection->save();
            if ($this->isVirtual())
            {
                $this->getBillingAddress()->setPaymentMethod($giftCardPaymentObj !== false ? $giftCardPaymentCode : null);
            }
            else
            {
                $this->getShippingAddress()->setPaymentMethod($giftCardPaymentObj !== false ? $giftCardPaymentCode : null);
                $this->getBillingAddress()->setPaymentMethod(null);
            }
            // shipping totals may be affected by payment method
            if (!$this->isVirtual() && $this->getShippingAddress()) {
                $this->getShippingAddress()->setCollectShippingRates(true);
            }
            /*recollect the all to get correct grandtotal*/
            if ($doDelete)
            {
                $this->setTotalsCollectedFlag(false);
                $this->collectTotals()->save();
            }
        }

        return $giftCardPaymentObj;

    }

}
