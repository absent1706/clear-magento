<?php
class Teamwork_Giftcards_Model_Svs extends Mage_Core_Model_Abstract
{
    protected $_google_path    = 'https://www.google.com/accounts/ClientLogin';
    protected $_login_suffix    = '/_ah/login';
    protected $_api_suffix    = '/lrppoints/loyaltypoints';
    protected $_source_name    = 'magento';
    protected $_write_to_log    = false;
    protected $_key;
    protected $_auth_name;

    const PAYMENT_GIFTCARDS_EMAIL        = 'payment/teamwork_giftcards/email';
    const PAYMENT_GIFTCARDS_PASWSWD        = 'payment/teamwork_giftcards/passwd';
    const PAYMENT_GIFTCARDS_PATH        = 'payment/teamwork_giftcards/path';
    const PAYMENT_GIFTCARDS_SERVICE        = 'payment/teamwork_giftcards/service';
    const PAYMENT_GIFTCARDS_SOURCE        = 'payment/teamwork_giftcards/source';
    const PAYMENT_GIFTCARDS_TYPE        = 'payment/teamwork_giftcards/accoun_type';
    const PAYMENT_GIFTCARDS_AUTHKEY        = 'payment/teamwork_giftcards/authkey';
    const PAYMENT_GIFTCARDS_BALANCE        = 'payment/teamwork_giftcards/balance';

    public function _construct()
    {
//return;
        if(
            !Mage::getStoreConfig(self::PAYMENT_GIFTCARDS_EMAIL)    ||
            !Mage::getStoreConfig(self::PAYMENT_GIFTCARDS_PASWSWD)  ||
            !Mage::getStoreConfig(self::PAYMENT_GIFTCARDS_PATH)     ||
            !Mage::getStoreConfig(self::PAYMENT_GIFTCARDS_SERVICE)  ||
            !Mage::getStoreConfig(self::PAYMENT_GIFTCARDS_SOURCE)   ||
            !Mage::getStoreConfig(self::PAYMENT_GIFTCARDS_TYPE)
        )
        {
            Mage::throwException('Please, fill full payment informtion');
        }

        if(strtolower(substr(Mage::getStoreConfig(self::PAYMENT_GIFTCARDS_PATH),0,5)) == 'https')
        {
            $this->_auth_name = 'SACSID';
        }
        elseif(strtolower(substr(Mage::getStoreConfig(self::PAYMENT_GIFTCARDS_PATH),0,4)) == 'http')
        {
            $this->_auth_name = 'ACSID';
        }
        else
        {
            Mage::throwException('Please, choose HTTP or HTTPS protocol');
        }
        $this->checkKey();
    }

    protected function checkKey()
    {
        if($this->_key = Mage::getStoreConfig(self::PAYMENT_GIFTCARDS_AUTHKEY))
        {
            $params = array(
                'requesttype' => '5',
                'deviceId' => $this->getname()
            );
            if($this->api($params) == 'Online')
            {
                return;
            }
        }
        $this->_key = $this->getNewKey();
    }

    protected function getNewKey()
    {
        $params = array(
            'accountType'   =>    Mage::getStoreConfig(self::PAYMENT_GIFTCARDS_TYPE),
            'Email'         =>    Mage::getStoreConfig(self::PAYMENT_GIFTCARDS_EMAIL),
            'Passwd'        =>    Mage::getStoreConfig(self::PAYMENT_GIFTCARDS_PASWSWD),
            'service'       =>    Mage::getStoreConfig(self::PAYMENT_GIFTCARDS_SERVICE),
            'source'        =>    Mage::getStoreConfig(self::PAYMENT_GIFTCARDS_SOURCE)
        );

        $http = new Varien_Http_Adapter_Curl();
            $http->setConfig(array('timeout' => 20, 'header' => 0));
            $http->write(Zend_Http_Client::POST, $this->_google_path, '1.1', array(), $params);
            parse_str(str_replace("\n", '&', $http->read()), $result);
        $http->close();

        if($result['Auth'])
        {
            $http = new Varien_Http_Adapter_Curl();
                $http->setConfig(array('header' => 1));
                $path = rtrim(Mage::getStoreConfig(self::PAYMENT_GIFTCARDS_PATH), '/') . "{$this->_login_suffix}?auth={$result['Auth']}";
                $http->write(Zend_Http_Client::GET, $path, '1.1');
                preg_match('/^Set-Cookie: '.$this->_auth_name.'=(.*?);/m', $http->read(), $result);
            return $this->setNewKey($result[1]);
        }
        elseif($result['Error'])
        {
            Mage::throwException($result['Error']);
        }
    }

    public function isGiftcardExists($giftcardNo)
    {
//return true;
        $params = array(
            'requesttype'   => '10',
            'GiftCardID'    => $giftcardNo,
            'deviceId'      => $this->getname()
        );
        return $this->api($params);
    }

    public function giftcardBalance($giftcardNo)
    {
//return 400;
        $params = array(
            'requesttype'   => '7',
            'GiftCardID'    => $giftcardNo,
            'deviceId'      => $this->getname()
        );
        $r = $this->api($params);
        return $r;
    }

    public function refund($transactionId, $storeId)
    {
        $params = array(
            'requesttype'            => '12',
            'gcTransactionId'        => $transactionId,
            'receiptLineId'            => $this->createGuid(),
            'localTransactionDate'    => date('Y-m-d\TH:i:s.000P'),
            'transactionSource'        => $this->_source_name,
            'deviceID'                => $this->getname(),
            'deviceGuid'            => $this->getChannelIdByStoreId($storeId),
        );
        return $this->api($params);
    }

    public function generateGiftCard($amount, $format=null)
    {
        $params = array(
            'requesttype'       => '122',
            'amount'            => (float)$amount,
            'receiptLineId'     => $this->createGuid(),
            'transactionSource' => $this->_source_name,
            'deviceID'          => $this->getname(),
            'deviceGuid'        => $_SERVER['SERVER_NAME'],
        );
        return $this->api($params);
    }

    public function executeOperation($giftcardNo, $amount, $negative=true, $storeId)
    {
        if($amount == 0)
        {
            array('error' => false, 'transactionId' => true);
        }

        $amount = $this->negative($amount, $negative);
        $params = array(
            'requesttype'           => '8',
            'giftCardId'            => $giftcardNo,
            'amount'                => $amount,
            'overdraft'             => (bool)Mage::getStoreConfig(self::PAYMENT_GIFTCARDS_BALANCE),
            'receiptLineId'         => $this->createGuid(),
            'eComm'                 => 'True',
            'localTransactionDate'  => date('Y-m-d\TH:i:s.000P'),
            'transactionSource'     => $this->_source_name,
            'deviceId'              => $this->getname(),
            'deviceGuid'            => $this->getChannelIdByStoreId($storeId),
        );

        if($amount<0)
        {
            $params['lineType'] = '2';
            $params['transactionLineType'] = '2';
        }
        else
        {
            $params['lineType'] = '3';
            $params['transactionLineType'] = '1';
        }

        $transactionId = $this->api($params);
        $params = array(
            'requesttype'        => '9',
            'gcTransactionId'    => $transactionId,
            'deviceId'           => $this->getname()
        );
        $return = $this->api($params);
        return stripos($return, 'error') === false ? array('error' => false, 'transactionId' => $transactionId) : array('error' => true, 'meaning' => $return);
    }

    public function addCard($giftcardNo, $amount, $negative=false)
    {
        return $this->executeOperation($giftcardNo, $amount, $negative, 1);
    }

    protected function api($params, $showHeader=0)
    {
        $header = array(
            'Content-type: application/x-www-form-urlencoded; charset=utf-8',
            'Cookie: '.$this->_auth_name.'='.$this->_key
        );

        $http = new Varien_Http_Adapter_Curl();
        $http->setConfig(array('header' => $showHeader));
        $http->write(Zend_Http_Client::POST, rtrim(Mage::getStoreConfig(self::PAYMENT_GIFTCARDS_PATH), '/') . $this->_api_suffix, '1.1', $header, http_build_query($params));
        $return = $this->prepareAnswer($http->read());

        if($showHeader == 0 && stripos($return, 'HTTP/1.1') == 0)
        {
            $response = explode("\r\n\r\n", $return);
            $return = end($response);
        }

        if($this->_write_to_log)
        {
            Mage::log($msg, null, 'teamwork_giftcards.log');
        }
        return $return;
    }

    protected function setNewKey($key)
    {
        Mage::getModel('core/config')->saveConfig(self::PAYMENT_GIFTCARDS_AUTHKEY, $key);
        Mage::app()->getCacheInstance()->cleanType('config');
        Mage::dispatchEvent('adminhtml_cache_refresh_type', array('type' => 'config'));
        return $key;
    }

    protected function getname()
    {
        if(function_exists('gethostname'))
        {
            return gethostname();
        }
        else
        {
            return php_uname('n');
        }
    }

    protected function prepareAnswer($string)
    {
        if(substr($string, -1) == ';')
        {
            return substr($string, 0, -1);
        }
        else
        {
            return $string;
        }
    }

    public function createGuid($namespace = '')
    {
        static $guid = '';
        $uid = uniqid("", true);
        $data = $namespace;
        $data .= !empty($_SERVER['REQUEST_TIME']) ? $_SERVER['REQUEST_TIME'] : '';
        $data .= !empty($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : '';
        $data .= !empty($_SERVER['SERVER_ADDR']) ? $_SERVER['SERVER_ADDR'] : '';
        $data .= !empty($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '';
        $data .= !empty($_SERVER['REMOTE_PORT']) ? $_SERVER['REMOTE_PORT'] : '';
        $hash = strtolower(hash('ripemd128', $uid . $guid . md5($data)));
        $guid = '' .
                substr($hash, 0, 8) .
                '-' .
                substr($hash, 8, 4) .
                '-' .
                substr($hash, 12, 4) .
                '-' .
                substr($hash, 16, 4) .
                '-' .
                substr($hash, 20, 12) .
                '';
        return $guid;
    }

    protected function isBoolean($value)
    {
        if($value && strtolower($value) === 'true')
        {
            return true;
        }
        else
        {
            return $value;
        }
    }

    protected function negative($amount, $negative)
    {
        if($negative && $amount > 0)
        {
            return '-'.$amount;
        }
        return $amount;
    }

    public function getFullRequestInformation($http)
    {
        echo '<pre>';
        var_dump('CURLINFO_EFFECTIVE_URL: ' . $http->getInfo(CURLINFO_EFFECTIVE_URL));
        var_dump('CURLINFO_HTTP_CODE: ' . $http->getInfo(CURLINFO_HTTP_CODE));
        var_dump('CURLINFO_FILETIME: ' . $http->getInfo(CURLINFO_FILETIME));
        var_dump('CURLINFO_TOTAL_TIME: ' . $http->getInfo(CURLINFO_TOTAL_TIME));
        var_dump('CURLINFO_NAMELOOKUP_TIME: ' . $http->getInfo(CURLINFO_NAMELOOKUP_TIME));
        var_dump('CURLINFO_CONNECT_TIME: ' . $http->getInfo(CURLINFO_CONNECT_TIME));
        var_dump('CURLINFO_PRETRANSFER_TIME: ' . $http->getInfo(CURLINFO_PRETRANSFER_TIME));
        var_dump('CURLINFO_STARTTRANSFER_TIME: ' . $http->getInfo(CURLINFO_STARTTRANSFER_TIME));
        var_dump('CURLINFO_REDIRECT_COUNT: ' . $http->getInfo(CURLINFO_REDIRECT_COUNT));
        var_dump('CURLINFO_REDIRECT_TIME: ' . $http->getInfo(CURLINFO_REDIRECT_TIME));
        var_dump('CURLINFO_SIZE_UPLOAD: ' . $http->getInfo(CURLINFO_SIZE_UPLOAD));
        var_dump('CURLINFO_SIZE_DOWNLOAD: ' . $http->getInfo(CURLINFO_SIZE_DOWNLOAD));
        var_dump('CURLINFO_SPEED_DOWNLOAD: ' . $http->getInfo(CURLINFO_SPEED_DOWNLOAD));
        var_dump('CURLINFO_SPEED_UPLOAD: ' . $http->getInfo(CURLINFO_SPEED_UPLOAD));
        var_dump('CURLINFO_HEADER_SIZE: ' . $http->getInfo(CURLINFO_HEADER_SIZE));
        var_dump('CURLINFO_HEADER_OUT: ' . $http->getInfo(CURLINFO_HEADER_OUT));
        var_dump('CURLINFO_REQUEST_SIZE: ' . $http->getInfo(CURLINFO_REQUEST_SIZE));
        var_dump('CURLINFO_SSL_VERIFYRESULT: ' . $http->getInfo(CURLINFO_SSL_VERIFYRESULT));
        var_dump('CURLINFO_CONTENT_LENGTH_DOWNLOAD: ' . $http->getInfo(CURLINFO_CONTENT_LENGTH_DOWNLOAD));
        var_dump('CURLINFO_CONTENT_LENGTH_UPLOAD: ' . $http->getInfo(CURLINFO_CONTENT_LENGTH_UPLOAD));
        var_dump('CURLINFO_CONTENT_TYPE: ' . $http->getInfo(CURLINFO_CONTENT_TYPE));
    }

    protected function getChannelIdByStoreId($storeId)
    {
        foreach (Mage::app()->getWebsites() as $website)
        {
            foreach ($website->getGroups() as $group)
            {
                $stores = $group->getStores();
                foreach ($stores as $store)
                {
                    if ($store->getId() == $storeId)
                    {
                        $select = Mage::getSingleton('core/resource')->getConnection('core_write')->select()
                            ->from(array(Mage::getSingleton('core/resource')->getTableName('service_channel')), array('channel_id'))
                        ->where('channel_name = ?', $store->getCode());

                        $channel_id = Mage::getSingleton('core/resource')->getConnection('core_write')->fetchOne($select);
                        return $channel_id;
                    }
                }
            }
        }
    }
}
