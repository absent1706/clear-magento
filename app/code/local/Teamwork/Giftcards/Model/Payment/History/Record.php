<?php

class Teamwork_Giftcards_Model_Payment_History_Record extends Mage_Core_Model_Abstract
{
    const RECORD_TYPE_OFFLINE_PAY    = "offline_pay";
    const RECORD_TYPE_OFFLINE_REFUND = "offline_refund";
    const RECORD_TYPE_ONLINE_PAY     = "online_pay";
    const RECORD_TYPE_ONLINE_REFUND  = "online_refund";

    const OPERATION_STATUS_SUCCESS  = "success";
    const OPERATION_STATUS_FAIL  = "fail";

    static public function validateType($type)
    {
        $allowedTypes = array(
            self::RECORD_TYPE_OFFLINE_PAY,
            self::RECORD_TYPE_OFFLINE_REFUND,
            self::RECORD_TYPE_ONLINE_PAY,
            self::RECORD_TYPE_ONLINE_REFUND,
        );
        return in_array($type, $allowedTypes);
    }

    static public function validateStatus($status)
    {
        $allowedStatuses = array(
            self::OPERATION_STATUS_SUCCESS,
            self::OPERATION_STATUS_FAIL,
        );
        return in_array($status, $allowedStatuses);
    }

    /**
     * Initialize resources
     */
    protected function _construct()
    {
        $this->_init('teamwork_giftcards/payment_history_record');
    }

    static public function getTypeOptionArray()
    {
        $helper = Mage::helper('teamwork_giftcards');
        return array(
            self::RECORD_TYPE_ONLINE_PAY => $helper->__('Online Pay'),
            self::RECORD_TYPE_ONLINE_REFUND => $helper->__('Online Refund'),
            self::RECORD_TYPE_OFFLINE_PAY => $helper->__('Offline Pay'),
            self::RECORD_TYPE_OFFLINE_REFUND => $helper->__('Offline Refund'),
        );
    }

    static public function getStatusOptionArray()
    {
        $helper = Mage::helper('teamwork_giftcards');
        return array(
            self::OPERATION_STATUS_SUCCESS => $helper->__('SUCCESS'),
            self::OPERATION_STATUS_FAIL => $helper->__('FAIL'),
        );
    }

}
