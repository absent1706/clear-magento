<?php

class Teamwork_Giftcards_Model_Payment_History extends Mage_Core_Model_Abstract
{
    static public function addRecord($orderId, $type, $amount, $status, $giftCardNo, $comment = '', $invoiceId = null, $creditmemoId = null)
    {
        if (Teamwork_Giftcards_Model_Payment_History_Record::validateType($type))
        {
            if (Teamwork_Giftcards_Model_Payment_History_Record::validateStatus($status))
            {
                $recordObj = Mage::getModel('teamwork_giftcards/payment_history_record');
                $recordObj->setData('type', $type);
                $recordObj->setData('order_id', $orderId);
                $recordObj->setData('amount', $amount);
                $recordObj->setData('comment', $comment);
                $recordObj->setData('status', $status);
                $recordObj->setData('gift_card_no', $giftCardNo);
                $recordObj->setData('invoice_id', $invoiceId);
                $recordObj->setData('creditmemo_id', $creditmemoId);
                $recordObj->save();
                $result = true;
            }
            else
            {
                throw new Exception(Mage::helper('teamwork_giftcards')->__('Teamwork giftcard history error: wrong status used (%s)', $status));
            }
        }
        else
        {
            throw new Exception(Mage::helper('teamwork_giftcards')->__('Teamwork giftcard history error: wrong type used (%s)', $type));
        }
    }
}
