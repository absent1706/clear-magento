<?php
class Teamwork_Giftcards_Model_Payment extends Mage_Payment_Model_Method_Abstract
{
    protected $_code                        = 'teamwork_giftcards';
    protected $_formBlockType               = 'teamwork_giftcards/form_giftcards';
    protected $_infoBlockType               = 'teamwork_giftcards/info_giftcards';
    protected $_negativeBalance             = 'payment/teamwork_giftcards/balance';

    protected $_isGateway                   = true;
    protected $_canFetchTransactionInfo     = true;
    protected $_canOrder                    = true;
    protected $_canAuthorize                = true;
    protected $_canCapture                  = true;
    protected $_canCapturePartial           = true;
    protected $_canRefund                   = true;
    protected $_canRefundInvoicePartial     = true;
    protected $_canVoid                     = true;
    protected $_canUseInternal              = false;
    protected $_canUseCheckout              = true;
    protected $_canUseForMultishipping      = false;
    protected $_canCreateBillingAgreement   = true;
    protected $_canReviewPayment            = true;


    public function assignData($data)
    {
        if (!($data instanceof Varien_Object)) {
            $data = new Varien_Object($data);
        }
        $info = $this->getInfoInstance();
        $info->setAdditionalInformation(array('GiftcardNo' => trim($data->getGiftcardNo()), 'Amount'=>trim($data->getAmount())) );
        return $this;
    }

    public function validate()
    {
//return $this;
        parent::validate();
        $info = $this->getInfoInstance();
        $giftcardNo = $info->getAdditionalInformation('GiftcardNo');

        if(empty($giftcardNo)) {
            $errorCode = 'invalid_data';
            $errorMsg = $this->_getHelper()->__('Gift Card number is required');
        }
        if(!empty($errorMsg)) {
            Mage::throwException($errorMsg);
        }
        $amount = $info->getAdditionalInformation('Amount');
        if(empty($amount)) {
            $errorCode = 'invalid_data';
            $errorMsg = $this->_getHelper()->__('Amount is required and should be greater then 0');
        }
        if(!empty($errorMsg)) {
            Mage::throwException($errorMsg);
        }
        $api = Mage::getSingleton('teamwork_giftcards/svs');
        $exists = $api->isGiftcardExists($giftcardNo);
        if($exists != 'true')
        {
            Mage::throwException("Gift Card Does Not Exist");
        }
        /*check amount*/
        $amount = $info->getAdditionalInformation('Amount');
        $balance = $api->giftcardBalance($giftcardNo);
        if ($balance < $amount)
        {
            Mage::throwException("Insufficient Balance");
        }
        return $this;
    }

    public function capture(Varien_Object $payment, $amount)
    {
//Mage::getSingleton('teamwork_giftcards/payment_history')->addRecord($payment->getOrder()->getId(), Teamwork_Giftcards_Model_Payment_History_Record::RECORD_TYPE_ONLINE_PAY, $amount, Teamwork_Giftcards_Model_Payment_History_Record::OPERATION_STATUS_SUCCESS, $this->getInfoInstance()->getAdditionalInformation('GiftcardNo'));
//Mage::helper('teamwork_giftcards')->rememberOnlineOperation($payment->getOrder(), Teamwork_Giftcards_Model_Payment_History_Record::RECORD_TYPE_ONLINE_PAY, $amount, Teamwork_Giftcards_Model_Payment_History_Record::OPERATION_STATUS_SUCCESS, $this->getInfoInstance()->getAdditionalInformation('GiftcardNo'));
//return $this;
        $giftcardNo = $this->getInfoInstance()->getAdditionalInformation('GiftcardNo');
        $api = Mage::getSingleton('teamwork_giftcards/svs');

        $balance = $api->giftcardBalance($giftcardNo);
        if($balance == '0.0')
        {
            $exists = $api->isGiftcardExists($giftcardNo);
            if($exists != 'true')
            {
                Mage::throwException("Gift Card Does Not Exist");
            }
        }
        if($balance >= $amount || ($balance < $amount && Mage::getStoreConfig($this->_negativeBalance) == 1))
        {
            $operation = $api->executeOperation($giftcardNo, $amount, true, $payment->getOrder()->getStoreId());
            if(!$operation['error'])
            {
				/* fix: string below is useless and caused bugs (it cleared important payment info)*/
                //$payment->setAdditionalInformation(array('GiftcardNo' => $giftcardNo));
                $payment->setStatus(self::STATUS_APPROVED)
                    ->setTransactionId($operation['transactionId'])
                ->setIsTransactionClosed(0);
            }
            else
            {
                //Mage::getSingleton('teamwork_giftcards/payment_history')->addRecord($payment->getOrder()->getId(), Teamwork_Giftcards_Model_Payment_History_Record::RECORD_TYPE_ONLINE_PAY, $amount, Teamwork_Giftcards_Model_Payment_History_Record::OPERATION_STATUS_FAIL, $giftcardNo, 'Please try later or contact us about the problem');
                Mage::helper('teamwork_giftcards')->rememberOnlineOperation($payment->getOrder(), Teamwork_Giftcards_Model_Payment_History_Record::RECORD_TYPE_ONLINE_PAY, $amount, Teamwork_Giftcards_Model_Payment_History_Record::OPERATION_STATUS_FAIL, $giftcardNo, 'Please try later or contact us about the problem');
                Mage::throwException('Please try later or contact us about the problem');
            }
        }
        else
        {
            //Mage::getSingleton('teamwork_giftcards/payment_history')->addRecord($payment->getOrder()->getId(), Teamwork_Giftcards_Model_Payment_History_Record::RECORD_TYPE_ONLINE_PAY, $amount, Teamwork_Giftcards_Model_Payment_History_Record::OPERATION_STATUS_FAIL, $giftcardNo, "Insufficient Balance");
            Mage::helper('teamwork_giftcards')->rememberOnlineOperation($payment->getOrder(), Teamwork_Giftcards_Model_Payment_History_Record::RECORD_TYPE_ONLINE_PAY, $amount, Teamwork_Giftcards_Model_Payment_History_Record::OPERATION_STATUS_FAIL, $giftcardNo, "Insufficient Balance");
            Mage::throwException("Insufficient Balance");
        }
        //Mage::getSingleton('teamwork_giftcards/payment_history')->addRecord($payment->getOrder()->getId(), Teamwork_Giftcards_Model_Payment_History_Record::RECORD_TYPE_ONLINE_PAY, $amount, Teamwork_Giftcards_Model_Payment_History_Record::OPERATION_STATUS_SUCCESS, $giftcardNo);
        Mage::helper('teamwork_giftcards')->rememberOnlineOperation($payment->getOrder(), Teamwork_Giftcards_Model_Payment_History_Record::RECORD_TYPE_ONLINE_PAY, $amount, Teamwork_Giftcards_Model_Payment_History_Record::OPERATION_STATUS_SUCCESS, $giftcardNo);
        return $this;
    }

    public function refund(Varien_Object $payment, $amount, $comment = "")
    {
//Mage::getSingleton('teamwork_giftcards/payment_history')->addRecord($payment->getOrder()->getId(), Teamwork_Giftcards_Model_Payment_History_Record::RECORD_TYPE_ONLINE_REFUND, $amount, Teamwork_Giftcards_Model_Payment_History_Record::OPERATION_STATUS_SUCCESS, $this->getInfoInstance()->getAdditionalInformation('GiftcardNo'));
//Mage::helper('teamwork_giftcards')->rememberOnlineOperation($payment->getOrder(), Teamwork_Giftcards_Model_Payment_History_Record::RECORD_TYPE_ONLINE_REFUND, $amount, Teamwork_Giftcards_Model_Payment_History_Record::OPERATION_STATUS_SUCCESS, $this->getInfoInstance()->getAdditionalInformation('GiftcardNo'), $comment);
//return $this;
        $api = Mage::getSingleton('teamwork_giftcards/svs');
        $error = true;
        if($amount != $payment->getAmountOrdered())
        {
            $operation = $api->executeOperation($payment->getAdditionalInformation('GiftcardNo'), $amount, false, $payment->getOrder()->getStoreId());
            if(empty($operation['error']) && !empty($operation['transactionId']))
            {
                $error = false;
                $transactionId = $operation['transactionId'];
            }
        }
        else
        {
            $response = $api->refund($payment->getRefundTransactionId(), $payment->getOrder()->getStoreId());
            if(substr($response, 0, 9) != 'Exception')
            {
                $error = false;
                $transactionId = $response;
            }
        }

        if($error)
        {
            //Mage::getSingleton('teamwork_giftcards/payment_history')->addRecord($payment->getOrder()->getId(), Teamwork_Giftcards_Model_Payment_History_Record::RECORD_TYPE_ONLINE_REFUND, $amount, Teamwork_Giftcards_Model_Payment_History_Record::OPERATION_STATUS_FAIL, $this->getInfoInstance()->getAdditionalInformation('GiftcardNo'), "Please try later or contact us about the problem.");
            Mage::helper('teamwork_giftcards')->rememberOnlineOperation($payment->getOrder(), Teamwork_Giftcards_Model_Payment_History_Record::RECORD_TYPE_ONLINE_REFUND, $amount, Teamwork_Giftcards_Model_Payment_History_Record::OPERATION_STATUS_FAIL, $this->getInfoInstance()->getAdditionalInformation('GiftcardNo'), $comment . Mage::helper('teamwork_giftcards')->__(" Please try later or contact us about the problem."));
            Mage::throwException(Mage::helper('teamwork_giftcards')->__("Please try later or contact us about the problem. "));
        }
        else
        {
            $payment->setStatus(self::STATUS_APPROVED)
                ->setTransactionId((string)$transactionId)
            ->setIsTransactionClosed(0);
        }
        //Mage::getSingleton('teamwork_giftcards/payment_history')->addRecord($payment->getOrder()->getId(), Teamwork_Giftcards_Model_Payment_History_Record::RECORD_TYPE_ONLINE_REFUND, $amount, Teamwork_Giftcards_Model_Payment_History_Record::OPERATION_STATUS_SUCCESS, $this->getInfoInstance()->getAdditionalInformation('GiftcardNo'));
        Mage::helper('teamwork_giftcards')->rememberOnlineOperation($payment->getOrder(), Teamwork_Giftcards_Model_Payment_History_Record::RECORD_TYPE_ONLINE_REFUND, $amount, Teamwork_Giftcards_Model_Payment_History_Record::OPERATION_STATUS_SUCCESS, $this->getInfoInstance()->getAdditionalInformation('GiftcardNo'), $comment);
        return $this;
    }
}