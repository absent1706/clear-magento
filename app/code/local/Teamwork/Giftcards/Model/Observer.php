<?php

class Teamwork_Giftcards_Model_Observer //extends Mage_Core_Model_Abstract
{
    /*always use giftcard after order placing if it present in the order and it is not the only payment method order uses*/
    public function salesOrderPaymentPlaceStart($observer)
    {
        $observer->getEvent()->getPayment()->payByGiftCard(true);
    }

    public function salesOrderInvoicePay($observer)
    {
        $invoice = $observer->getEvent()->getInvoice();
        $giftcardsUsedBeforeWhileCapture = $invoice->getData('___tw_giftcards_used_by_capture_');
        if (!$giftcardsUsedBeforeWhileCapture)
        {
            $invoice->getOrder()->getPayment()->payByGiftCard(true, $invoice->getGrandTotal());
        }
    }

    public function salesOrderInvoiceSaveAfter($observer)
    {
        $invoice = $observer->getEvent()->getInvoice();
        $orderId = $invoice->getOrderId();

        $this->_saveOperationsByTypes(
                array(
                    Teamwork_Giftcards_Model_Payment_History_Record::RECORD_TYPE_OFFLINE_PAY,
                    Teamwork_Giftcards_Model_Payment_History_Record::RECORD_TYPE_ONLINE_PAY
                ),
                $orderId,
                $invoice->getId(),
                null
        );
    }

    public function salesOrderCreditmemoSaveAfter($observer)
    {
        $creditmemo = $observer->getEvent()->getCreditmemo();
        $orderId = $creditmemo->getOrderId();

        $this->_saveOperationsByTypes(
                array(
                    Teamwork_Giftcards_Model_Payment_History_Record::RECORD_TYPE_OFFLINE_REFUND,
                    Teamwork_Giftcards_Model_Payment_History_Record::RECORD_TYPE_ONLINE_REFUND
                ),
                $orderId,
                null,
                $creditmemo->getId()
        );
    }

    protected function _saveOperationsByTypes($operationTypes, $orderId, $invoiceId, $creditMemoId)
    {
        if (!is_array($operationTypes)) $operationTypes = array($operationTypes);
        foreach($operationTypes as $operationType)
        {
            if (!Teamwork_Giftcards_Model_Payment_History_Record::validateType($operationType)) continue;
            if ($operations = Mage::helper('teamwork_giftcards')->getRememberedOfflineOperations($orderId, $operationType))
            {
                $this->_saveOperations($operations, $invoiceId, $creditMemoId);
            }
        }
    }

    protected function _saveOperations($operations, $invoiceId, $creditmemoId)
    {
        foreach($operations as $operation)
        {
            Mage::getSingleton('teamwork_giftcards/payment_history')->addRecord($operation['order_id'], $operation['type'], $operation['amount'], $operation['status'], $operation['gift_card_no'], $operation['comment'], $invoiceId, $creditmemoId);
        }
    }

    /*check whether we already added giftcard payment and if grandtotal changed - update amount*/
    public function salesQuoteCollectTotalsAfter($observer)
    {
        $quote = $observer->getEvent()->getQuote();
        $savedGrandTotal = $quote->getGiftCardAdditionalInfo('GrandTotal');
        $savedAmount = $quote->getGiftCardAdditionalInfo('Amount');
        $newGrandTotal = $quote->getGrandTotal();

        if ($savedGrandTotal && $savedAmount && $savedGrandTotal != $newGrandTotal)
        {

            $maxAllowedAmount = $quote->getGiftCardAdditionalInfo('MaxAllowedAmount');

            if ($maxAllowedAmount === false || is_null($maxAllowedAmount))
            {
                $maxAllowedAmount = $savedAmount;
            }
            if ($newGrandTotal < $savedGrandTotal)
            {
                if ($newGrandTotal < $savedAmount)
                {
                    $savedAmount = $newGrandTotal;
                }
            }
            else
            {
                if ($newGrandTotal > $savedAmount)
                {
                    $savedAmount = $newGrandTotal;
                }
                if ($savedAmount > $maxAllowedAmount)
                {
                    $savedAmount = $maxAllowedAmount;
                }
            }

            $quote->setGiftCardAdditionalInfo('Amount', $savedAmount);
            $quote->setGiftCardAdditionalInfo('MaxAllowedAmount', $maxAllowedAmount);
            $quote->setGiftCardAdditionalInfo('GrandTotal', $newGrandTotal);
        }
    }

    /*copy gc payment to order*/
    public function salesModelServiceQuoteSubmitBefore($observer)
    {
        $quote = $observer->getEvent()->getQuote();
        $payments = $quote->getPaymentsCollection();
        if (count($payments) > 1)
        {
            $giftCardPayment = $quote->getGiftCardPayment();
            if (!$giftCardPayment)
            {
                throw new Exception(Mage::helper("teamwork_giftcards")->__("Wrong payments set"));
            }
            $order = $observer->getEvent()->getOrder();
            $converter = Mage::getModel('sales/convert_quote');
            $order->addPayment($converter->paymentToOrderPayment($giftCardPayment));
        }
    }

    /*return cash to GC if order creation failed*/
    public function salesModelServiceQuoteSubmitFailure($observer)
    {
        $order = $observer->getEvent()->getOrder();
        $payments = $order->getPaymentsCollection();
        $giftCardPayment = $order->getGiftCardPaymenMethod(false);
        if ($giftCardPayment)
        {
            $instance = $giftCardPayment->getMethodInstance();
            $infoInstance = $instance->getInfoInstance();
            $paid = $infoInstance->getAdditionalInformation('Paid');
            $amount = $infoInstance->getAdditionalInformation('Amount');
            if ($paid && $amount)
            {
                /*return back record about GC online payment (not saved due exception and rolling back in MYSQL transaction)*/
                Mage::getSingleton('teamwork_giftcards/payment_history')->addRecord(null, Teamwork_Giftcards_Model_Payment_History_Record::RECORD_TYPE_ONLINE_PAY, $amount, Teamwork_Giftcards_Model_Payment_History_Record::OPERATION_STATUS_SUCCESS, $infoInstance->getAdditionalInformation('GiftcardNo'), Mage::helper("teamwork_giftcards")->__("Order creation failed"), null, null);
                /*return back cash to GC and adding correspond record to history table*/
                $instance->refund($giftCardPayment, $amount, Mage::helper('teamwork_giftcards')->__("Order creation failed"));
            }
        }
    }
    /*correct grand total and add "GC" item*/
    public function paypalPrepareLineItems($observer)
    {
        $paypalCart = $observer->getEvent()->getPaypalCart();
        $object = $paypalCart->getSalesEntity();
        $amount = false;
        $giftCardNo = false;
        /*check whether GC is in use and get amount*/
        if ($object instanceof Mage_Sales_Model_Quote)
        {
            $amount = $object->getGiftCardAdditionalInfo('Amount');
            $giftCardNo = $object->getGiftCardAdditionalInfo('GiftcardNo');
        }
        else if ($object instanceof Mage_Sales_Model_Order)
        {
            if ($giftCardPayment = $object->getGiftCardPaymenMethod())
            {
                $infoinstance = $giftCardPayment->getMethodInstance()->getInfoInstance();
                $amount = $infoinstance->getAdditionalInformation('Amount');
                $giftCardNo = $infoinstance->getAdditionalInformation('GiftcardNo');
            }
        }
        if ($amount && strlen($giftCardNo))
        {
            $paypalCart->addItem(Mage::helper('teamwork_giftcards')->__('Giftcard'), 1, -1.00 * $amount, $giftCardNo);
//            $paypalCart->updateTotal(Mage_Paypal_Model_Cart::TOTAL_DISCOUNT, $amount);
            //$paypalCart->updateTotal(Mage_Paypal_Model_Cart::TOTAL_SUBTOTAL, -1.00 * $amount);
        }

    }

}
