<?php

class Teamwork_Giftcards_Model_Resource_Payment_History_Record_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract//Mage_Core_Model_Resource_Db_Collection_Abstract
{
    protected function _construct()
    {
        $this->_init('teamwork_giftcards/payment_history_record');
    }
}
