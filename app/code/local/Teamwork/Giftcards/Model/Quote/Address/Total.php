<?php

class Teamwork_Giftcards_Model_Quote_Address_Total extends Mage_Sales_Model_Quote_Address_Total_Abstract
{

    public function collect(Mage_Sales_Model_Quote_Address $address)
    {
        return $this;
    }

    /**
     * Add grand total information to address
     *
     * @param   Mage_Sales_Model_Quote_Address $address
     * @return  Mage_Sales_Model_Quote_Address_Total_Grand
     */
    public function fetch(Mage_Sales_Model_Quote_Address $address)
    {
        $quote = $address->getQuote();
        //if ($address->getAddressType() == Mage_Customer_Model_Address_Abstract::TYPE_SHIPPING && Mage::helper('teamwork_giftcards')->getGiftCardAdditionalInfo('GiftcardNo') !== false)
        $shippingType = Mage::helper('teamwork_giftcards')->getClassConstVal('Mage_Customer_Model_Address_Abstract', 'TYPE_SHIPPING', 'shipping');
        if ($address->getAddressType() == $shippingType && $quote->getGiftCardAdditionalInfo('GiftcardNo') !== false)
        {
            $address->addTotal(array(
                'code'  => $this->getCode(),
                'title' => Mage::helper('teamwork_giftcards')->__('Gift Card Amount'),
                //'value' => Mage::helper('teamwork_giftcards')->getGiftCardAdditionalInfo('Amount'),
                'value' => $quote->getGiftCardAdditionalInfo('Amount'),
                /*'area'  => 'footer',*/
            ));
        }
        return $this;
    }
}
