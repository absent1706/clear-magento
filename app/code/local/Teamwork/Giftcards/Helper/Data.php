<?php

class Teamwork_Giftcards_Helper_Data extends Mage_Core_Helper_Abstract
{
    protected $_paymentModelCode = null;
    const CONFIG_PATH_MODEL_CODE = "payment/teamwork_giftcards/model";

    const OFFLINE_OPERATION_GLOBAL_KEY = '__teamwork_giftcards_offline_operation_cache_';
    const ONLINE_OPERATION_GLOBAL_KEY = '__teamwork_giftcards_online_operation_cache_';

    public function getMethodCode()
    {
        if (is_null($this->_paymentModelCode))
        {
            $this->_paymentModelCode = false;
            $model = Mage::getStoreConfig(self::CONFIG_PATH_MODEL_CODE);
            if (!empty($model))
            {
                $model = Mage::getModel($model);
                if ($model/* instanceof Mage_Payment_Model_Method_Abstract*/)
                {
                    $this->_paymentModelCode = $model->getCode();
                }
            }
        }
        return $this->_paymentModelCode;
    }

    public function rememberOfflineOperation($orderId, $type, $amount, $status, $giftCardNo, $comment = '')
    {
        $this->_rememberOperation(self::OFFLINE_OPERATION_GLOBAL_KEY, $orderId, $type, $amount, $status, $giftCardNo, $comment);
    }

    public function getRememberedOfflineOperations($orderId, $type)
    {
        return $this->_getRememberedOperations(self::OFFLINE_OPERATION_GLOBAL_KEY, $orderId, $type);
    }



    public function rememberOnlineOperation($order, $type, $amount, $status, $giftCardNo, $comment = '')
    {
        if (!$order->theOnlyGiftCardPaymentInUse())
        {
            Mage::getSingleton('teamwork_giftcards/payment_history')->addRecord($order->getId(), $type, $amount, $status, $giftCardNo, $comment);
        }
        else
        {
            $this->_rememberOperation(self::ONLINE_OPERATION_GLOBAL_KEY, $order->getId(), $type, $amount, $status, $giftCardNo, $comment);
        }
    }

    public function getRememberedOnlineOperations($orderId, $type)
    {
        return $this->_getRememberedOperations(self::ONLINE_OPERATION_GLOBAL_KEY, $orderId, $type);
    }


    protected function _rememberOperation($key, $orderId, $type, $amount, $status, $giftCardNo, $comment)
    {
        if ($operationCache = Mage::registry($key))
        {
            Mage::unregister($key);
        }
        if (!is_array($operationCache)) $operationCache = array();
        if (!isset($operationCache[$orderId]) || !is_array($operationCache[$orderId])) $operationCache[$orderId] = array();
        if (!isset($operationCache[$orderId][$type]) || !is_array($operationCache[$orderId][$type])) $operationCache[$orderId][$type] = array();
        $operationCache[$orderId][$type][] = array('order_id' => $orderId, 'type' => $type, 'amount' => $amount, 'status' => $status, 'gift_card_no' => $giftCardNo, 'comment' => $comment);
        Mage::register($key, $operationCache);
    }

    public function _getRememberedOperations($key, $orderId, $type)
    {
        $result = Mage::registry($key);
        if (isset($result[$orderId][$type]) && is_array($result[$orderId][$type])) $result = $result[$orderId][$type];
        else $result = array();
        if (!is_array($result)) $result = array();
        return $result;
    }

    public function getClassConstVal($className, $constName, $defaultVal = null)
    {
        $ref = new ReflectionClass($className);
        $consts = $ref->getConstants();
        return isset($consts[$constName]) ? $consts[$constName] : $defaultVal;
    }
}