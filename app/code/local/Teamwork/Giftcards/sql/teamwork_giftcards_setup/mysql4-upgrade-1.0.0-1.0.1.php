<?php
$installer = $this;
$installer->startSetup();
$orderTableName = $this->getTable('sales/order');
$tableName = $this->getTable('teamwork_giftcards/payment_history');

$installer->run("
DROP TABLE IF EXISTS `{$tableName}`;
CREATE TABLE `{$tableName}` (
`entity_id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `order_id` INT(10) UNSIGNED NOT NULL,
  `invoice_id` INT(10) UNSIGNED DEFAULT NULL,
  `creditmemo_id` INT(10) UNSIGNED DEFAULT NULL,
  `type` VARCHAR(255) DEFAULT '',
  `status` VARCHAR(255) NOT NULL,
  `gift_card_no` VARCHAR(255) NOT NULL,
  `amount` DECIMAL(12,4) NOT NULL DEFAULT 0,
  `comment` TEXT DEFAULT NULL,
  PRIMARY KEY (`entity_id`),
  FOREIGN KEY (`order_id`) REFERENCES `{$orderTableName}` (`entity_id`) ON UPDATE CASCADE ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");

$installer->endSetup();
