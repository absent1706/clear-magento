<?php
$installer = $this;
$installer->startSetup();
$adapter = $installer->getConnection();
$tableName = $this->getTable('teamwork_giftcards/payment_history');
$orderTableName = $this->getTable('sales/order');
$res = $adapter->fetchRow("SHOW CREATE TABLE {$tableName}");
$error = true;
if (isset($res['Create Table']))
{
    if (preg_match("/CONSTRAINT\s+([a-z0-9_`]+)\s+FOREIGN KEY\s+\(`?order_id`?\)\s+REFERENCES\s+`?{$orderTableName}`?\s\(`?entity_id`?\)\s+ON DELETE\sCASCADE\sON UPDATE\sCASCADE/", $res['Create Table'], $prRes))
    {
        if (!empty($prRes[1]))
        {
            $installer->run("ALTER TABLE `{$tableName}` DROP FOREIGN KEY $prRes[1]");
            $installer->run("ALTER TABLE `{$tableName}` DROP KEY `order_id`");
            $installer->run("ALTER TABLE `{$tableName}` CHANGE COLUMN `order_id` `order_id` INT(10) UNSIGNED NULL DEFAULT NULL AFTER `created_at`");
            $error = false;
        }
    }
}

if ($error)
{
    throw new Exception("Error occured while Teamwork Giftcards module installation/updating");
}

$installer->endSetup();
